<?php

ini_set('display_errors', 1);

class Compiler {

    private $assets = [];
    private $assetIndex = 0;
    private $path;
    
    private $varChar = 97;
    private $varIndex = 0;

    public function init() {
        echo '<pre>';
        $this->path = __DIR__ . '/';
        echo "Compile assets\n";
        $this->compileAssets([
            '*/*.js',
            '*/*.css',
            '*/*/*.js',
            '*/*/*.css',
        ]);
        
        echo "Unit code to one file\n";
        $this->unitOneFile('index.php', 'saccess.php');
        
        echo "Minify PHP\n";
        $this->minifyPHP('saccess.php', 'saccess.php');
        
        echo "Creating the archive\n";
        $this->makeArchive(['config.default.php', 'saccess.php', 'adminer.php'], 'saccess.zip');
    }

    private function compileAssets($paths = []) {
        $len = strlen($this->path);
        $vars = [];
        foreach ($paths as $mask) {
            foreach (glob($this->path . $mask, GLOB_NOSORT) as $path) {
                $relPath = substr($path, $len);
                $code = file_get_contents($path);
                if (substr($path, -3) === '.js') {
                    preg_match_all('/\$([A-Za-z]+)/s', $code, $variables, PREG_PATTERN_ORDER);
                    $variables = array_unique($variables[1]);
                    foreach ($variables as $variable) {
                        if (isset($vars[$variable])) {
                            $var = $vars[$variable];
                        } else {
                            $var = $vars[$variable] = $this->getVar();
                        }                    
                        $code = preg_replace('/\$' . $variable . '\b/s', $var, $code);
                    }
                }
                // make it into one long line
                //$code = str_replace(array("\n", "\r"), ' ', $code);
                // replace all multiple spaces by one space
                $code = preg_replace('!\s+!', ' ', $code);
                // replace some unneeded spaces, modify as needed
                $code = str_replace(array(' {', ' }', '{ ', '; '), array('{', '}', '{ ', ';'), $code);
                $this->assets[$relPath] = $code;
                echo "\t\t$relPath\n";
            }
        }
    }

    private function unitOneFile($source, $target) {
        $target = $this->path . $target;
        $code = "<?php \r\n";
        $code .= 'class Assets {';
        foreach ($this->assets as $key => $value) {
        	$code .= "\n    const A" . md5($key) . " = '" . str_replace("'", '\\\'', $value) . "';\n"; 
        }
        $code .= '};';
        $code .= $this->_includePHPFile($this->path . $source);
        $r = file_put_contents($target, $code);
        echo $r ? 'Succefull' : 'Failed', " united in $target\n";
    }

    private function minifyPHP($source, $target) {
        $source = $this->path . $source;
        $target = $this->path . $target;
        $code = php_strip_whitespace($source);
        //$code = file_get_contents($source);
        
        // classes
        preg_match_all('/class\s+([A-Za-z]+)/s', $code, $classes, PREG_PATTERN_ORDER);
        foreach ($classes[1] as $class) {
            $var = $this->getVar(true);
            $code = preg_replace('/class\s+\b' . $class . '\b/s', 'class ' . $var, $code);
            $code = preg_replace('/new\s+\b' . $class . '\b/s', 'new ' . $var, $code);
            $code = preg_replace('/\b' . $class . '\b::/s', $var . '::', $code);
            $code = preg_replace('/extends\s+' . $class . '/s', 'extends ' . $var, $code);
        }
        $this->resetVar();
        
        // variables
        preg_match_all('/\$([A-Za-z]+)/s', $code, $variables, PREG_PATTERN_ORDER);
        $variables = array_unique($variables[1]);
        foreach ($variables as $variable) {
            $var = $this->getVar();
            if (in_array($variable, ['GLOBALS', 'this'])) {
                continue;
            }
            $code = preg_replace('/\$' . $variable . '\b/s', '$' . $var, $code);
            $code = preg_replace('/->' . $variable . '\b([^\(])/s', '->' . $var . '$1', $code);
            $code = preg_replace('/(addModule\([^\)]+[\'"])(' . $variable . ')([\'"]\s*=>)/s', '$1' . $var . '$3', $code);
        }
        
        // methods
        preg_match_all('/(public|protected|private)\s+function\s+([A-Za-z]+)\(/s', $code, $functions, PREG_PATTERN_ORDER);
        $functions = array_unique($functions[2]);
        foreach ($functions as $function) {
            if (in_array($function, ['credentials', 'database', 'head', 'name'])) {
                continue;
            }
            $var = $this->getVar();
            $code = preg_replace('/(public|protected|private)\s+function\s+' . $function . '\(/s', '$1 function ' . $var . '(', $code);
            $code = preg_replace('/->' . $function . '\(/s', '->' . $var . '(', $code);
        }
        
        // static methods
        preg_match_all('/(public|protected|private)\s+static\s+function\s+([A-Za-z]+)\(/s', $code, $functions, PREG_PATTERN_ORDER);
        $functions = array_unique($functions[2]);
        foreach ($functions as $function) {
            $var = $this->getVar();
            $code = preg_replace('/(public|protected|private)\s+static\s+function\s+' . $function . '\(/s', '$1 static function ' . $var . '(', $code);
            $code = preg_replace('/::' . $function . '\(/s', '::' . $var . '(', $code);
        }
        $this->resetVar();
        
        // constants
        preg_match_all('/\sconst\s+([0-9a-zA-Z_]{4,})/s', $code, $constants, PREG_PATTERN_ORDER);
        $constants = array_unique($constants[1]);
        foreach ($constants as $constant) {
            $var = '_' . $this->getVar(true);
            $code = preg_replace('/\b' . $constant . '\b/s', $var, $code);
        }
        
        $result = file_put_contents($target, $code);
        echo $result ? 'Succefull' : 'Failed', " minify into $target\n";
    }

    private function _includePHPFile($path, $html = false) {
        if (!$rows = file($path, $html ? FILE_IGNORE_NEW_LINES : 0)) {
            echo "\t\t<b>Include error: $path</b>\n";
            return '';
        }
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        echo "\t\tInclude " .($html ? 'layout' : 'class') . ": $path\n";
        $code = $html ? '?>' : "\r\n";
        foreach ($rows as $key => $row) {
            if (!$html && !$key)
                continue;
            if (preg_match("%include '(.*)'.*%", $row, $matches)) {
                $code .= $this->_includePHPFile($this->path . '/' . $matches[1], true);
            } elseif (preg_match("%^include_once '(.*)'%", $row, $matches)) {
                $code .= $this->_includePHPFile($dir . '/' . $matches[1], false);
            } elseif (preg_match("%^(.*)file_get_contents\('(.*)'\);.*$%", $row, $matches)) {
                $code .= $matches[1] . 'Assets::A' . md5($matches[2]) . ';';
            } else {
                $code .= $html ? trim($row) : $row;
            }
        }
        if ($html) {
            $code .= '<?php ';
        }
        return $code;
    }
    
    private function getVar($caps = false) {
        $code = $this->varChar;
        if ($caps) {
            $code -= 32;
        }
        $var = chr($code);
        if ($this->varIndex) {
            $var .= $this->varIndex;
        }
        
        $this->varChar++;
        if ($this->varChar > 122) {
            $this->varChar = 97;
            $this->varIndex++;
        }
        return $var;
    }
    
    private function resetVar() {
        $this->varChar = 97;
        $this->varIndex = 0;
    }

    private function makeArchive($files, $target) {
        try {
            $zip = new \ZipArchive;
            if ($zip->open($this->path . $target, \ZipArchive::CREATE) !== true) {
                echo "<b>Create $target archive error</b>\n";
                return;
            }
            foreach ($files as $file) {

                if (is_file($this->path . $file)) {
                    $zip->addFile($this->path . $file, $file);
                    echo "\t\tZip $file\n";
                }
            }
            $zip->close();
            echo "Create archive $target\n";
        } catch (Exception $e) {
            echo "<b>{$e->getMessage()}</b>\n";
        }
    }

}

$compiler = new Compiler();
$compiler->init();