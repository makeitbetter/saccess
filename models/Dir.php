<?php

/**
 * Directory
 */
class Dir {

    /**
     * @var array<Directory> Collection
     */
    public static $collection;

    /**
     * @const Access denied message
     */
    const ACCESS_DENIED_MESSAGE = 'Access to *d denied.';
    
    
    /**
     * @var string Directory
     * @var string Name
     * @var string Extension
     * @var string Path
     * @var boolean Is this module directory
     * @var boolean Is link
     */
    public $dir, $name, $extension, $path, $isSaccess, $isLink;
    
    /**
     * @var string Type
     * @var string Icons CSS class
     * @var int Date modified
     * @var int File size
     * @var boolean Is accessable
     */
    protected $type, $iconClass, $modifyTime, $fileSize, $isAccessable;

    /**
     * Factory
     * @param string $path Path
     * @return object
     */
    public static function getItem($path) {
        if (isset(static::$collection[$path])) {
            return static::$collection[$path];
        }
        return static::$collection[$path] = new static($path);
    }

    /**
     * Constructor
     * @param string $path Path
     */
    protected function __construct($path) {
        $this->path = $this->normalizePath($path);
        if (in_array($this->path, [SACCESS, __FILE__, SACCESS . I. 'adminer.php', SACCESS . I. 'config.php'])) {
            $this->isSaccess = 1;
        }
        if (is_link(ROOT . $this->path)) {
            $this->isLink = true;
        }
        $info = pathinfo($this->path);
        $this->name = $info['basename'];
        $this->extension = isset($info['extension']) ? $info['extension'] : null;
        if ($path === '/') {
            $this->dir = '';
            $this->name = '';
        } elseif (empty($info['dirname'])) {
            $this->dir = null;
        } elseif (preg_match('%^([a-z]:)$%i', $this->path, $matches)) {
            $this->dir = null;
            $this->name = $matches[1];
        } else {
            $this->dir = $info['dirname'];
        }
    }

    /**
     * Normilize path
     * @param string $path
     * @return string
     */
    public function normalizePath($path) {
        $left = ($path[0] === '/') ? '/' : '';

        $items = explode(I, trim($path, I));
        $list = [];
        foreach ($items as $item) {
            if (($item == '.') || strlen($item) === 0) {
                continue;
            }
            if ($item == '..') {
                array_pop($list);
            } else {
                array_push($list, $item);
            }
        }
        return $left . implode(I, $list);
    }

    /**
     * Is directory exists
     * @return boolean
     */
    public function exists() {
        return is_dir($this->path . '/');
    }

    /**
     * Create directory
     * @return boolean
     */
    public function create() {
        if (!$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        if (!is_dir($this->path)) {
            return mkdir($this->path, 0777, true);
        }
        return true;
    }

    /**
     * Read directory
     * @param boolean $asArray Return as array
     * @return array [d=>array<D>, f=>array<F>] or array<D>
     */
    public function read($asArray = false) {
        $result = ['dirs' => [], 'files' => []];
        if (!$this->exists() || !$this->hasAccess()) {
            return [];
        }
        $dir = opendir($this->path);
        $parent = $this->getParent();
        $result['parent'] = ['path' => $parent ? $parent->path : '', 'access' => $parent && $parent->hasAccess(), 'name' => $parent && $parent->name ? $parent->name : 'ROOT'];
        while ($item = readdir($dir)) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            /* @var $entity Dir */
            $type = is_dir($this->path . I . $item) ? 'dirs' : 'files';
            $entity = $type === 'dirs' ? Dir::getItem($this->path . I . $item) : File::getItem($this->path . I . $item);
            $result[$type][] = $asArray ? ['name' => $item, 'size' => $entity->getFileSize(), 'time' => $entity->getModifyTime(), 'icon' => $entity->getIcon(), 'type' => $entity->getType(), 'ext' => $entity->extension, 'access' => $entity->hasAccess(), 'path' => urlencode($entity->path)] : $entity;
        }
        closedir($dir);
        return $result;
    }

    /**
     * Is accessable
     * @param boolean $checkNested Check nested directories
     * @return boolean
     */
    public function hasAccess($checkNested = false) {
        if ($this->isAccessable !== null) {
            return $this->isAccessable;
        }
        if (User::isGuest()) {
            return $this->isAccessable = false;
        }
        if ($this->exists() && !is_readable($this->path)) {
            return $this->isAccessable = false;
        }
        $isAccessable = User::hasAccess();
        foreach ($isAccessable['deny'] as $deny) {
            $path = $checkNested ? strpos($deny, $this->path) : strpos($this->path, $deny);
            if ($path === 0) {
                return $this->isAccessable = false;
            }
        }
        if (empty($isAccessable['access'])) {
            return $this->isAccessable = true;
        }
        foreach ($isAccessable['access']as $deny) {
            if (strpos($this->path, $deny) === 0) {
                return $this->isAccessable = true;
            }
        }
        return $this->isAccessable = false;
    }

    /**
     * Copy directory
     * @param string $newName New name
     * @param boolean $useFullPath Use full path instead relative
     * @return boolean
     */
    public function copy($newName, $useFullPath = false) {
        if (!$this->hasAccess(true) || !$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        $path = $useFullPath ? $newName : $this->dir . '/' . $newName;
        $directory = Dir::getItem($path);
        if (!$directory->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $directory->path]), 'danger');
            return false;
        }
        if (!$directory->exists()) {
            $directory->create();
        }
        $content = $this->read();
        foreach ($content['files'] as $useFullPath) {
            $useFullPath->copy($directory->path . '/' . $useFullPath->name, true);
        }
        foreach ($content['dirs'] as $useFullPath) {
            $useFullPath->copy($directory->path . '/' . $useFullPath->name, true);
        }
        return true;
    }

    /**
     * Rename directory
     * @param string $newName New name
     * @return boolean
     */
    public function rename($newName) {
        if (!$this->hasAccess(true) || !$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        $directory = Dir::getItem($this->dir . '/' . $newName);
        if (!$directory->hasAccess(true) || !$directory->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $directory->path]), 'danger');
            return false;
        }
        return rename($this->path, $directory->path);
    }

    /**
     * Delete directory
     * @return boolean
     */
    public function delete() {
        if (!$this->hasAccess(true) || !$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        $content = $this->read();
        $error = false;
        foreach ($content['files'] as $file) {
            if (!$file->delete()) {
                $error = true;
            }
        }
        foreach ($content['dirs'] as $directory) {
            $directory->delete();
        }
        if ($error) {
            return false;
        }
        return rmdir($this->path);
    }

    /**
     * Get type
     * @return string
     */
    public function getType() {
        return 'directory';
    }

    /**
     * Get icon CSS class
     * @return string
     */
    public function getIcon() {
        if ($this->isLink) {
            return 'link';
        }
        $isSaccess = $this->isSaccess ? ' saccess' : '';
        if (!$this->hasAccess()) {
            return 'folder-close' . $isSaccess;
        }
        return 'folder-open' . $isSaccess;
    }

    /**
     * Get parent directory
     * @return Dir
     */
    public function getParent() {
        return $this->dir == null ? false : Dir::getItem($this->dir);
    }

    /**
     * Get date modified
     * @return int
     */
    public function getModifyTime() {
        return $this->modifyTime ?: $this->modifyTime = @filemtime($this->path);
    }

    /**
     * Get file size
     * @return int
     */
    public function getFileSize() {
        return $this->fileSize ?: $this->fileSize = @filesize($this->path);
    }

    /**
     * Get URI
     * Path relative DOCUMENT_ROOT
     * @return string
     */
    public function getUri() {
        if (strpos($this->path, ROOT) !== 0) {
            return null;
        }
        $path = I=== '\\' ? str_replace('\\', '/', $this->path) : $this->path;
        return $this->path == ROOT ? '' : substr($path, strlen(ROOT));
    }

}