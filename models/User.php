<?php

/**
 * Current user
 */
class User {

    /**
     * @const Break between authentications in seconds
     */
    const AUTH_PAUSE = 5;
    
    /**
     * @var string User identifier/login/name
     * @var array User data
     */
    private static $userId, $info;

    /**
     * Initiate
     */
    public static function init() {
        $userId = Request::getSession('u');
        if ($info = App::config("users/$userId")) {
            self::$userId = $userId;
            self::$info = $info;
        }
    }

    /**
     * Get user identifier
     * @return string
     */
    public static function getId() {
        return self::$userId;
    }

    /**
     * Is guest
     * @return boolean
     */
    public static function isGuest() {
        return !self::$userId;
    }

    /**
     * Access parameters
     * @return array
     */
    public static function hasAccess() {
        return [
            'access' => isset(self::$info['access']) ? (array) self::$info['access'] : [],
            'deny' => isset(self::$info['deny']) ? (array) self::$info['deny'] : [],
        ];
    }

    /**
     * Login
     * @param string $userName Identifier
     * @param string $password Password
     * @return boolean
     */
    public static function login($userName, $password) {
        $users = App::config('users');
        $error = false;
        if (Request::getSession('p') && Request::getSession('p') >= time()) {
            $error = true;
        } elseif (!isset($users[$userName])) {
            $error = true;
        } else {
            $hash = is_string($users[$userName]) ? $users[$userName] : (isset($users[$userName]['password']) ? $users[$userName]['password'] : null);
            if (!$password || md5($password) !== $hash) {
                $error = true;
            }
        }
        if ($error) {
            App::addAlerts('Authentication failed.<br/>Try again after ' . self::AUTH_PAUSE . 's', 'danger');
            Request::setSession('p', time() + self::AUTH_PAUSE);
        } else {
            Request::setSession('u', $userName);
            Request::setSession('p', null);
        }
        return !$error;
    }

    /**
     * Logout
     */
    public static function logout() {
        Request::setSession('u', null);
    }

}
