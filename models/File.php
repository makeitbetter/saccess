<?php

/**
 * File
 */
class File extends Dir {

    /**
     * @var array<File> Collection
     */
    public static $collection;

    /**
     * @var array Extensions
     */
    private static $extensions = [];
    
    protected function __construct($path) {
    	parent::__construct($path);
   	}

    /**
     * Is file exists
     * @return boolean
     */
    public function exists() {
        return is_file($this->path);
    }

    /**
     * Create file
     * @return boolean
     */
    public function create() {
        if (!$this->getParent()->exists()) {
            $this->getParent()->create();
        }
        return file_put_contents($this->path, null, FILE_APPEND) !== false;
    }

    /**
     * Read file
     * @return string
     */
    public function read($asArray = false) {
        return $this->exists() ? file_get_contents($this->path) : false;
    }

    /**
     * Write file
     * @param string $string content
     * @return boolean
     */
    public function write($string) {
        return $this->hasAccess() ? file_put_contents($this->path, trim($string)) !== false : false;
    }

    /**
     * Get type
     * @return string
     */
    public function getType() {
        if (isset(self::$extensions[$this->extension])) {
            return self::$extensions[$this->extension];
        }
        foreach (App::config('file_types', []) as $type => $extensions) {
            if (in_array($this->extension, $extensions)) {
                return self::$extensions[$this->extension] = $type;
            }
        }
        return self::$extensions[$this->extension] = 'file';
    }

    /**
     * Get icon CSS class
     * @return string
     */
    public function getIcon() {
        if ($this->isLink) {
            return 'link';
        }
        $type = $this->getType();
        if ($this->name[0] === '.') {
            return 'cog';
        }
        switch ($type) {
            case 'image':
            case 'audio':
            case 'video': return $type;
            default: return 'file' . ($this->isSaccess ? ' saccess' : '');
        }
    }

    /**
     * Get handler module
     * @return string
     */
    public function getHandler() {
        $type = $this->getType();
        if (in_array($type, ['php', 'txt', 'xml', 'html', 'js', 'css', 'sql'])) {
            return 'syntax';
        }
        if (in_array($type, ['image', 'video', 'audio'])) {
            return $type;
        }
    }

    /**
     * Rename file
     * @param string $newName New name
     * @return boolean
     */
    public function rename($newName) {
        if (!$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        $file = File::getItem($this->dir . '/' . $newName);
        if (!$file->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $file->path]), 'danger');
            return false;
        }
        return rename($this->path, $file->path);
    }

    /**
     * Copy file
     * @param string $newName New name
     * @param boolean $useFullPath Use full path instead relative
     * @return boolean
     */
    public function copy($newName, $useFullPath = false) {
        if (!$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        $path = $useFullPath ? $newName : $this->dir . '/' . $newName;
        $useFullPath = File::getItem($path);
        if (!$useFullPath->getParent()->exists()) {
            $useFullPath->getParent()->create();
        }
        if (!$useFullPath->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $useFullPath->path]), 'danger');
            return false;
        }
        return copy($this->path, $useFullPath->path);
    }

    /**
     * Delete file
     * @return boolean
     */
    public function delete() {
        if (!$this->hasAccess()) {
            App::addAlerts(Text::format(self::ACCESS_DENIED_MESSAGE, ['*d' => $this->path]), 'danger');
            return false;
        }
        return unlink($this->path);
    }

}