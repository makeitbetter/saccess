<?php

/**
 * Request
 */
class Request {

    /**
     * @var string Sessions and Cookies prefix
     */
    private static $prefix = 'saccess_';

    /**
     * Define prefix
     */
    public static function init() {
        if (session_status() !== 2) {
            session_start();
        }
    }

    /**
     * Get GET requests parameter
     * @param string $param Parameter name
     * @param mixed $default Default value
     * @return mixed
     */
    public static function getQueryParam($param, $default = null) {
        return isset($_GET[$param]) ? $_GET[$param] : $default;
    }

    /**
     * Get POST requests parameter or (without arguments) Request method is POST
     * @param string $param Parameter name
     * @param mixed $default Default value
     * @return mixed
     */
    public static function getPostData($param = null, $default = null) {
        if (!$param) {
            return $_SERVER['REQUEST_METHOD'] === 'POST';
        }
        return isset($_POST[$param]) ? $_POST[$param] : $default;
    }

    /**
     * Get uploaded file data
     * @param string $param Parameter name
     * @return object|false
     */
    public static function getFiles($param) {
        if (!isset($_FILES[$param])) {
            return false;
        }
        $obj = new StdClass();
        $obj->name = $_FILES[$param]['name'];
        $obj->type = $_FILES[$param]['type'];
        $obj->tmp_name = $_FILES[$param]['tmp_name'];
        $obj->error = $_FILES[$param]['error'];
        $obj->size = $_FILES[$param]['size'];
        return $obj;
    }

    /**
     * Get Cookie parameter
     * @param string $param Parameter name
     * @param mixed $default Default value
     * @return mixed
     */
    public static function getCookie($param, $default = null) {
        return isset($_COOKIE[self::$prefix . $param]) ? $_COOKIE[self::$prefix . $param] : $default;
    }

    /**
     * Set Cookie parameter
     * @param string $param Parameter name
     * @param mixed $value Parameter value
     * @param mixed $$expire Expire date (timestamp or string for strtotime())
     */
    public static function setCookie($param, $value, $expire = null) {
        if (is_string($expire)) {
            $expire = strtotime($expire);
        }
        setcookie(self::$prefix . $param, $value, $expire);
    }

    /**
     * Get Session parameter
     * @param string $param Parameter name
     * @param mixed $default Default value
     * @return mixed
     */
    public static function getSession($param, $default = null) {
        return isset($_SESSION[self::$prefix . $param]) ? $_SESSION[self::$prefix . $param] : $default;
    }

    /**
     * Set Session parameter
     * @param string $param Parameter name
     * @param mixed $value Parameter value
     */
    public static function setSession($param, $value) {
        $_SESSION[self::$prefix . $param] = $value;
    }

    /**
     * Push to Sessions array parameter
     * @param string $param Parameter name
     * @param type $value Parameter value
     */
    public static function appendSession($param, $value) {//push session
        $_SESSION[self::$prefix . $param][] = $value;
    }

    /**
     * Update query string parameters
     * @param mixed $data
     *   "param1=value1&param2=value2"
     *   ["param1=value1", "param2=value2"]
     *   ["param1" => "value1", "param2" => "value2]
     * @return string
     */
    public static function updateQueryParams($data = null) {
        if (!$data) {
            return $_SERVER['QUERY_STRING'];
        }
        $current = $_GET;
        if (is_string($data)) {
            parse_str($data, $params);
        } else {
            $params = $data;
        }
        foreach ($params as $key => $value) {
            if (is_numeric($key)) {
                list($key, $value) = explode('=', $value, 2);
            }
            $current[$key] = $value;
        }
        return http_build_query($current);
    }

}