<?php

/**
 * Layout
 */
class Layout {

    /**
     * @var boolean Is initiated
     * @var array<string> CSS paths list
     * @var array<string> JS paths list
     */
    private static $isInitiated, $cssPaths = [], $jsPaths = [];

    /**
     * @var string Title
     */
    public static $title;

    /**
     * Initiate
     * @return void
     */
    public static function init() {
        if (self::$isInitiated) {
            return;
        }
        self::$title = $_SERVER['HTTP_HOST'];
        self::$isInitiated = true;
        self::$cssPaths[] = file_get_contents('styles/boot.css');
        self::$cssPaths[] = file_get_contents('styles/layout.css');
        self::$cssPaths[] = 'header {background-color: ' . App::config('color', '#003') . ';}'; 
        ob_start();
        self::$jsPaths[] = file_get_contents('scripts/main.js');
        return;
    }

    /**
     * Add CSS
     * @param string $path File path
     */
    public static function addCss($path) {
        self::$cssPaths[] = $path;
    }

    /**
     * Add JS
     * @param string $path File path
     */
    public static function addJs($path) {
        self::$jsPaths[] = $path;
    }
    
    /**
     * Returns link to host
     */
    public static function getHostLink() {
    	return '<a href="' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '">' 
            . $_SERVER['HTTP_HOST'] . '</a>';
    } 

    /**
     * Print header
     */
    private static function renederHeader() {
        include 'layout/header.php';
    }

    /**
     * Print menu
     */
    private static function renderMenu() {
        include 'layout/menu.php';
    }

    /**
     * Print modal window
     */
    private static function renderModal() {
        include 'layout/modal.php';
    }

    /**
     * Print footer
     */
    private static function renderFooter() {
        include 'layout/footer.php';
    }

    /**
     * Draw page
     * @return void
     */
    public static function renderPage() {
        if (!self::$isInitiated) {
            return;
        }
        $content = ob_get_contents();
        ob_clean();
        self::renederHeader();
        if (User::getId()) {
            include 'layout/page.php';
        } else {
            include 'layout/login.php';
        }
        self::renderFooter();
        ob_end_flush();
    }
    
    /**
     * @var string Logotype image base64
     */
    public static $logo = 'iVBORw0KGgoAAAANSUhEUgAAADwAAABMCAYAAAAx3tSzAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB98LDxEuIVqF9SgAABYhSURBVHja7XtpmF1lle67vn1ODakKmRgCLYRBQLCVbiUPEEAZW8RLN4ojCFfhduu1byuion21W+zuy0VBLqCNNCiigjRTO4AMGZgNhCkmzRhIQkISkqpUaq5z9t7fet/+sfepOpmYioTu57nfn1Pn1D7f3utb07vetQ7w/9dbv+Lw8H+tB05fWvmariO51f/lQ0O7AICk/xpCDz+5uJNp+glJ1zLGb0s6gnn8KN2/IvKLjLEh0A4bFi/cAQDiwOCYlkdGppE8/dUO5q0xwaGhLX7uQ0OtkvaS8wmSyz3PrmOW3UBytsg1JL/GND3CR0Zm1JataNlUo5Ruk7Tnf06/6+ubTvL/krzf6/Un6H6fpK9Teh/dH1Cx1jPGS+k+W5LoPsgYF0naV9KPSbYDwMilPwQAiLzdR2qHb+2ePjKyfYRrNrE8y4zuO4icUGrIJF0nSSRFUo1FNf1dfN4rcgHdv0yyn+RXm7XMGFdLOhgAfLDJ3AcHEQcHwxt59sob+VIIAZIS5PnBMtsP0pEwaxG5FpJBeJuCwcxQCjAAabWZJYBaYVa14vNVkNpAHoUkmQjgkwAuMjPEF5ZOsiSZLmkWgMdCZyd4/VXAKWcgH8ms0lY9FMD87SIwAOQDAxOqbW2JAV0wWyrg2zBrBwAzwAQX/VFIQwihE0AHAAnoMKAdZhVInZDWWgjLANwjs2Mk7WNmS5O99vyw3BdYCOdxcHCRmd1HCcEMzOrvVqjs/0YEttd6oaRRjZXmtosBu8vsTwGcYiEcK6Bihe/NAdmDJDkEQLsBkwC0S+oz4DaRC5TWH7f2CcMAPmQh7Cv3mWb2PMzeKelZC6FHefZdq7Y8BHKlgI+HJFkSu7qrYcdpdwyYHT/ZbNvmLEmB7h9hjItEjoisk8xIOiUpxvXMs8tFLmLhpKXTchk9fkrkBMVY4UgtNEXi45Tnsxnj7nK/QORzksQ0vYDuk9XXe2Tp838NAB7jbZJO2ubpKg4MVHxk5KNyP0X1+rEih0qhxCI6PcYYb9aYlCK5gdG/samllA97Gt13lvv5ZXB7R/n/v5Ukxngxyc9Q+gBjXCnp7xjjN0TO3ebCeu+GhhnPkPSXkp5tyCkpZ4zz6P5oKSxFuchFzPMzSb5P0vTNrIVcTPeXRN7OYrObSoEPLvag5C56PETkH0j+ilLGPP/gNhW49mIBE9nfd7TI60mONLRaauYuSc80NEtSjHE+Y36FpAclXUbpXSIPJzmrSdM/L83jDpJrWXx9hqR3kHy5NJGn0hVLWyTdJGm9yC66H7XNwcjw80snslb7gKRZIheVDyNKd7Dw19F8S3IVs+zHJCOj/67U5rEke0iuI/lPkhJm6RGSJPf7SC4ugchXWa8fX270B7nP8DQ9XlI/3XvpPlfkfEnnbMtA1Xjdk+Tvm4Rdxxh/rabF6JLH9zPPb5EkZtm3NDw0jTH2lYfxosglkiYyz9olzWOeL1We/y/m2T9Lepgery6vnS/3M0mmIutM03+j+ypJNZJTtymaEvkJlaY86rnuV5cm7GPenF9eHs55pbl+l+S3yq/1Mss+LvIgkZXhFSss1usHiryK5FARmeu/KA9TIh+S9B1Ky+l+g2JcVt7vsG2bisgTJX1XMR7DMVz4Iun3iuwWeTfdF5Fk7OttVEAzyyN4nlm2tnzQ32wU9Xs3vEvu8ygNcQxq/msprBTjLSpc5R6SPyG5zOu1I8Zw9PAkuu/JPJ/mcvj69a8NJb6KsO0ye8nMvo4Q3gkAAoAY58CdMHsKZocC6AT5nWTS5J5S4GhmQ5Lejmp1F0iZ3Oc09h16+eWdkok7fA5AYkAHSlAjaYoVGyxQCAfJDAbb38xWwf1cq7asGFm7ZprIU62l9WoA18JwqnWvryY77gjPs3GYdFMlQvJrkrLRwBTj7yWt3ag4iPE7TNOKpCDy500FgkRGkb9jlp3CF1+sDK5ePZUxzif5NN2faKoorqD7PLo/LCkt79WtPP+MqJsV43me598vr87l/jeS9vFa7Yw8re1eAhnowbvH5cs/lhRL4TiaI8v3Teno+tIqDm0SlpsAkZzkaQBA97MkDZKcI/c15SXLSK4kGcv3PXS/mOQCkheIPF/kvNLs1ynPj5R0KckNJJ+T+wdfjSUJW6FXCpOO8TRzTxXjg2YGMzOTyuqgeN8EyPcgeQGA2aMuAZiAiwyYWxQVVkGM1XztGoPrl3I+YcBxMvs1yPUC9jKz3Q14UTH+UNJcC+GLgJ6EdBjMdod0rxV7r1YILuA4M5tiZvvB7GrFeFQz5n/NKxsY6IxpelCptT+jtr7Isaq31PbTkr5B95Oaio2flJe8v3z/F8yyPZjHr4g8h9HXF24T/x/dryi1/yTJ72cvrd5J5LMkb6L7ISJF95sY41ksovoyRr/Ha7UTPcs++/pLv00om5K9EJur+S1JXbwspfs+GwW/GCskf0GyLulIr9UPVG3kAJKPijyd7pdRipI2MMaTmWVnSfqr/rVrp5YmurukR+i+hGn6d+Xt1pNcQXKAMe4NAHypy3xDX+vrroernZ0bBSwze1/pGCZJZpYCaCuDdlHLF+YeZfa5EMLSRjlJcgaASXCfCuBsT+sDVm39mpLKBkjzYLaHhbAvgETk/ZYkH0CSdAu4cdL06SWIJ2RGC2FftLT8g4pnmQZJID8dqtVljBGhUhGA1IeGkXR2vP60RPd9AfxPAMsKxwXM7Em5/0BSjyRrCG0AlKanBbO5m2yzI4CPIIRr2NV1ZWhp/ZxVkh4A91oIywXMlPSeMg9+m3neypGRHkQf43SKYBHU9NaAPgO+FKrV3wJAqIzpbmvCvhZY+TlJp9N1/ygfRV5ean5ZE8yUpK/6wEBSclo7Ne2RUGqXFCR9j+5/RfI8kt3Msp/JPW2K4l9jzO/zev1sr9X2bERbHxiYTI+3bpLqcpILJB27KQx+4ykpS2fqmac7STawcGSMn6d0uMhBkpFSv9yvZJ7vBAAcHqowTT8q6TCRifK8RdIkkleK/Hvv6z2E5EtlsbCEef4bSWKeXaUY75J0F91/6UNDR8TBgaSBB7K+gd0Y/UyRyyXlkuIoKpP+kvRp44OUWVoIXU/3IfkCSaf7k4zxZpLzRd7GPJ9H9x9JWtjIgWXyfxulm0T+XDH+UtKjlL7cxFefTvcukaT73DIqD5VA42nP0t8x5sc0tBZ7+5qUkE2l9BmR36H01ChA8fjEuASu9w00zGQ63WeSXEj3BZLqZfrpF7lCRaTsY57/bLNDi/FgSR8k2dFckEialq9ff4ry/FFJa+n+IsVe5nEOyZnM8o/JfR7T9KiNLE5qpzQaUZWmO5FcQkl0Lhl34dC/YnUrnaeJvFGFvzTXvaJ0j8gvkf4zkvdK+ma2dEX1FcvM6EXx0NPbwZ7uGcyyP/MsPZjih2LfwN6jwuX5SXSfozy/itLHKH1S7t8kueNG+7p/ioVlnD5ugQdWrjFK0+S+G8lvlsKuatQzzLMvUVogqaYYf8U0vYbk1T44uPtmNFFtDJvnPb2jf2+YM3eT9sxw8yFNZ559XtIlkr5M932VZSAJ9pcWWKsdpiy7UFIYl7C13v4iWDz+XDvJy5XnV0v6PN1vZYznyv17o8hKekTS002k3lLV63/MGN8p6ag4MvyOTfdffeHFW63BY8mhlYGzTLfcYhT2Wm0Hr9XttfJcWyXi26dMQlzbg8r0aTUAXyiDzbnWPmEioJrMZgGIAComtYtcCaBLSTJowIhaW28xckDkT0NSSUcF6Fmf2OTJsqSy0dMNXfpDhFAoKd5w0xhQaNk6cIojNSTt7QPNHZE3jwjI80TuXx+Fku6PU3qQ5MpRLkta2igZ6X5dI1czxltUpLKZqtVOVb0+8VVTom8bVvJ1lRVM0x1QqdxgIZxQ4szlktbALDFgOsz2HPM/LDBxGEC7isqqE9IMkPcgJF0SFzCt3RUqLW8zsz9BpXKg0vQRVKurzcIfLFjvWypwEzbe18jfKIQDyi9HAH0y64L7YjOjDIfDwgyT+gVEM0skTYYZIC1CnnehWm0DsGuJyXc16RGY3QrgRgth6ZbaO9u3/9vdjaaU0S5y7mgvpUhRLrIucoTkw4p+Dd1/IelXJJ/jxoVVlJRv1D7NsuslOaU1kr74aizqdjHpxg0lIYQAxngOgv0jYO3WpAo1bSwyt2APgHxBsokA1lkI75T8ELOkDVCQWcWkh1RYy66Q/tXMTgcwo6hMrAfAFZJ+EEJYNx7Nv2F7kTssSeDDw/tbS8t3rVJ5n4ApjfKpoQqzsRZf0yGstxDul3uwECYLmGFmO0saAPAzAM8bebxC2NlIVwjvATDNzF6C9AEL4RkNDcI6J24/gTcLaNJJkE406XiY7VMwjhtre0t9VwHrrWBIUjObiBAOhXS3gMUwO8sAE/kYgBYzmwmzpXrpxfeGPfYaeUt8e1OfYpa9m+5nlH2o9ZuOPpQk4Bgj1PjcvVfuf2D0OZLWS1qnPHuIMV4vKRM5QPJmSZTHc7bUMGBtZPtpWFkGa2lp5rQ7YTZB9fp+1tZ2jICZkN5vwESMjUIUfHSDNilSXZfcH0AI7zeziQIGDXgAwK4CZsLsagCTg9nHmw79QPVvWBUmTxvY7pFdg8NQzEHS6G6bWkD21OIJJGcpyz5G6UKSC0nmjcq2jPrdIn/NsvRT0ZGcLXIR3XNKx5Vx5OMk/71k1Ookz9zuJs7B4ZmK+f8geTndbxJ5IbP0SEkTN7+2mMRhjJNJXiZyXVM75wm631MeQEayTukmRl8t8n7GuBuLxpxIytP6kd7VvVfW27eTpDYA8Nc4ETgu7W4SyN6umH+LxRjDHJIflvvE0vf2EHmtyMvofgylhOQFlJaM0Th+mciayEfo/C3JRyldI6lO95PpvkiSPMbfy/0rjL6E5J0iv88NGya+ZYHMR0ZmSLqSZCZpDqW/pnTnKEVTgJXZLMiFuZKeLANcN/P835jWbyk1+e+ULhW5mHl+Cd3vkqTYs35Pkb2lbaySdB5rtbe/JVHca7VGNK1IOpXua5ro7R7GeCHdf9mEwmr0OIdkjyRxZOR8ut/YRPJfK/JukrNHTTqPF48Nv+lWkp2SWvBWL0rGNA2SjvehoT+XNNnr9RAlaGjwMLlfw5hfT+k4qpwKiD5b0mNs+DfZQ2mJyLvUgKxkJJnL/Sdy3+lNYzG3Sd4mwcH+ja8p0tr8Uph7WZjpPRpzgVTkV0ne2gTQh5hlh7yhZtq2XJvmyBACwsRJm4EDAbVyyG25AZ2SOiR5eSA15dldBnjTxhUkyeHps0+3lr3m8Qusrg2vOBrxZq2se107gM7SDyKASSAzFGOOgHQv8zgCs8PLYkYAWi2Ei1r2P+DLJCvjBh8bzXzE+GFP01O3lbmTPJpkL6VH5b64DFZLRC6mRLo/S/LBjQdq4jMih1U05T5StoHelJmPz6poQKeSzt6SlofXdm/94Ho3bDmaN4bfyF1UkIJiMUkwIOnxcpDmZhY1tySldF8m9x9KmsEsnzZKQbn/DdP6+N1V7nszz/+hTAeUNCipvUBLmcljIqkiyZbedl/CmJ2hPO8leTez9DPq3TDVJfOBAWNaD9n6tY2oHSQFDg3NlLSMRX1xg6S5nqY/ZoyLJS5gMfEzQjIT+U9bPNAYL3H349+U4iEODu6gGKuVyZMzSLcIOMqAz1oI1zFmHzSEk5EkfyryCcR4rVBZYFWbZWb3llukivFykA/B8G7Whp8KbR27W7VlXxlmmbQfgGGJ8wGbBuk5S5IzACxH0c55BElyukmzLUnO2SI54X66pMeSavWZcflt06aVoudU/0RZ1t2tkZEJkvZVmn6aeX79KOB3zmmgoS2sBSLvk/tjJJ+n9Czz7E5Gf0DSSsb4W7p7yYCuFv1SSVPiqlUdAKCBgc1TnNSiGCc0hlteFy8NAFi8uPCvLJtlabrOJkw4H0Ab8+ximLFMM7PU1jYPwAyrVh15fkvp6/fB0GEW+iQ9BbIXIaw1s8kiKwihCrP3wqyjHFWCKtX9y0N9yMz2RghB0rBivAHB7gjVpLfoUA7DOjo2U04wywBkZUfi9Zn0aK2a559WklwI4KcG/C2KHNdtZjUBe4CcgxCegvSsmZ2gPO+zanUnSe8C0GlALmBCyW6mZlYphgjMIS2Ae13SQ6hU/sTMzgCwStJaA/YUcA2EK3xouCWZUH0+VNviNq93SV5XRr5/5MZzSHXl+bmUDqD0U0m9cv8/DWJ+7NLRBnbz0MvYWIj77Yzxk2U9eyfT9BhJJ8i9g+4BAPKy7eNpfdvy0uXEzU8lGcweNLIO8he47bb7cfLJfwTpUpidIikHcL2ZnY4y/6kwrREzq4u+CCHpR56/DLOlVqkYY3wsVCoZPB4v6V2W5z+yCR0PbDJygZAk2xX3nqmilJsk8ndNdM7RIv+3YjyrHFC7tTHjXPRqfb7Iv1eM73ld99uK320aRLfZcDiz7ACm6aTytD/UdBC7lq+fJtmrojcsutcZ43mxr2/aNnCtxr133mYari9caPWFC4u/n38Bwz+6onl++kiSg5S62ZiI97HfN1BC/qN/eXOELYdGJR3EVwAV42utdHU1t1Y2Nr3u7ikkV6qYc95AaTnJfpFTAIADA9uASBieSvISkZPGs89W8WZl5zHLCdWNpxg0ZcpZAE6A2f2SUpDrzKxfZsXJXHPdm3Po68YweWib8EnEuJ612vB2L+zTwcEDS3DxhYKS8QVyl2I8ajxlY+3Rh8cKkDIVxRWrWtjV3UL330o6t4Hdt9vKmn69zSzbTWQm9wdJPU33bmXZf9sYf/e/UuXVIekwxfywvh9ctFmrf6CrZzqHRw5nnp8lskbpCpK7vTVUTZqCUoXkFeVPeu6W+50kX5L7VSx+JFlYxJqVk+h+HN2/qDwerdtvbaX7F1jQNi+LHGSW3SjpvzPG/coA9Rcxz8+W9ADdR0ow802SYTxpadwYjdLxVsxIS8CIud8Ns36EMAXk0zDbQ8AsmO2AGOeb2SMK4VMA3mZmE0TegSz7nrW2LlKWvtta246G9EciByxJviCpzYAnIZ0P8SarVON4nrcyLi339wfE+McK4XZIj5v056pUTkIjqJidCDNYg31IkhNUBDsrf07bb0ArWlq+B+C9aGl9jPX0wdDWurcBz7O/f59k8uQ1knDHhRcFkeOOVuPSMG+9FXbiiUchhF0l7WlmJ0t6wYAuAJBhqhHHyjDVgBEBKYCJAHKYPQ7J4b4QwN02ODjPpk6V19MDQ0v1SzI7xoBpyrIlqFavN3I5kmSOmdXeMoFLhuHtAPa+pFKZfbb7wxbCIeWYwJVer10Q2toPg/uhliQCtFLULkjCroDtAvcdLUmWK8aFCOEJxHwdgJ2RVJaEanXZ1hDXeMaTxi2wZ1krgJakpWXQB/t3twmd3we0HMJBMCPy/EFUKhMg7QAzmWEYQipxmiWVl1EEozpCGDTyBURfg9aWEQDdVtS3by5NPD7Il4+CEtZrCG3t8Hp9x7y3t9rS2VGzjs7d4N5BqUe1+jrV8rSyy9SdQVaY1mJo7+i2Blh5hZr8P+1qQNChZcvecCdiW6//AOwxVexwMTARAAAAAElFTkSuQmCC';
    
    /**
     * @var string Favicon image base64
     */
	public static $favicon = 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB98LDxE0GLWth/sAAARiSURBVFjDxdd/qJ5lHQbwz/c95+y0zQ3d1OaYblM3Z2mr552Ktqxs+QNtxSKpMEiMCiIGBSaCwtBMSv/ZH4IhQhIMgn6gRonKYDaHa++b22IZqJAuT565Wuc43c7pvF//uO9gYNjZuzN8/nnfh+d+7vt6vt/rvq7r5n2+or/X2pdE+GhmThB/GRoceH7yPzsnaBaiR/df051p4PgWbq7krJ8TK/A9XERs7PXyg2HxXswmljGyf7ozDk5z4RW4nVhJriLXEvvL+7kHa7MM3EJ+DjtolmM/3ckTaEF7Di7Dt8mPEKtIxEHyXjyLt4nrcTqWkmPEo+QR4vd0pk6gAjmEW3DjMZj3kVuwme4Ezd/ICboraCexmfxOef7ei0Pr/3B0PXHtMYBeI+eH2IFttK8jHsYwzR3kX8kesZZYOp3mvgeAZhEuwMLSqejhTmJbynuIl7AVR4kFuIk4A5fjUfKhQtr2YJ8AYqIQKpFZxsYT5BJcQg6Ts/FNchCv4c2ys3KU+B3xefJ6mtl9AMhPEmdVMFGA+Fkpt7uJ1cQiPIC9+BB5kNiOc8ggXy87w1f6IeEBck4FkxXEOcRl9fnb5DpiPsaIHeSv6DxC8zwuxHnEBRjth4RNFaoH6+RHg6tLzw2SO3AmuYy4Cn+vLUC8XD9ue9mOnaP9cODXxF5cjLPxYopXyeXkFXgzypjL8TLOL0LVnEJnA9YRLxJ/qoQ+7hYM4xCuIHcTU+T9Ie5InT9TWaG9GnMwTp5C3Er7H3iEzlaaI8RoH0rYDBNryB/iNMzDXKKLPeSDEXEgszN+kt2weRg315sJ4hA5VUgaL2BXbdGTdB47GQBaxL3k94lWLXzWd+tvHCBn4T5ikHyc7q4ZsuORZORJFj+DBZhf23HsB8ytfBkivlHEafFLjOw5CYGkWVJKHp8uBLUSK2oxelUzkJsHonXPVPY2EL+hM9qHGb1r8Q9Ugfky2aVzA66qVt2tcj1ADhCrp7L3FHF7Eaz2aTMYyaD9NbIhriRfIS7F6/hY5cmhGloOl5blF+m+MIMAoH1q0fw4UlwzP1vsOSmitQ+fKqLU/UOfkayZU0TpfwXNzqFjbvbTXlj/T2Jf4YYeNtGso5t9AIibcAPNl0oks4/8OLGc/CUxuyanq7GRPIA38OGajp4mNlbrfqsfHVhQDekn+AK+XkobnyH/XSPaqUUTYphcSvyYvIvukTrHPLrjx0nC9ifITSVe5bPEXPyzWvK8GjoPk28Va3Ym/oin8YuSF08sli+pE/6AuA7X1Dj+XFW8kZqIx8lWkeXO2Awempqh4aFLZ9VqbKJJmoempe+xZjBizdknGEq7k0cnd06E9nDJ++C305k0s/etzN7kDJ4N2+eSzxXnix/R2UZz3kC0Dk5lbz2xthW5mdYrvezdVs4ScS6dwzPoBc3WEjYFZpUInmOFkB4oipersXMgWj+dyl2jM3g2BN8lvopVda8vqhowTuyuGXE7xqb+G5amcb0DEpmJMYbTwhIAAAAASUVORK5CYII=';

}