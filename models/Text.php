<?php

/**
 * Text format
 */
class Text {

    /**
     * Format bytes value to string with units
     * @param int $bytes Bytes value
     * @param int $precision Number of decimal points.
     * @return string
     */
    public static function getBytes($bytes, $precision = 2) {
        if ($bytes < 1024) {
            return number_format($bytes, $precision, '.', ' ') . ' B';
        }
        if ($bytes < 1048576) {
            return number_format($bytes / 1024, $precision, '.', ' ') . ' KB';
        }
        if ($bytes < 1073741824) {
            return number_format($bytes / 1048576, $precision, '.', ' ') . ' MB';
        }
        return number_format($bytes / 1073741824, $precision, '.', ' ') . ' GB';
    }

    /**
     * Cut string by words to certain size.
     * @param string $string String
     * @param int $limit Max size
     * @return string
     */
    public static function limit($string, $limit) {
        if (strlen($string) > $limit) {
            return strrev(strstr(strrev(substr($string, 0, $limit)), ' ')) . '...';
        }
        return $string;
    }

    /**
     * Transliterate cyrillic symbols and special signs
     * @param string $string String
     * @return type
     */
    public static function transliterate($string) {
        $string = str_replace('"', '', $string);
        $string = str_replace("'", '', $string);
        $string = iconv('utf-8', 'windows-1251//TRANSLIT', $string);
        $string = str_replace('+', 'plus', $string);
        $string = str_replace('&', 'and', $string);
        $string = strtr($string, iconv('utf-8', 'windows-1251', 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'), 'abvgdeejziiklmnoprstufhzcss*y*euaABVGDEEJZIIKLMNOPRSTUFHZCSS*Y*EUA');
        $string = str_replace('*', '', $string);
        $string = preg_replace('|[^a-zA-Z0-9-_.]+|is', '-', $string);
        $string = preg_replace('|([-_.])+|is', '$1', $string);
        $string = trim($string, '-_.');
        return $string;
    }

    /**
     * Format string
     * @param string $string String
     * @param array $args Arguments
     * @param boolen $useSpecialSymbols Modify special signs
     * @return string
     */
    public static function format($string, $args = [], $useSpecialSymbols = true) {
        foreach ($args as $key => $value) {
            $value = self::specialChars($value);
            if ($useSpecialSymbols) {
                switch (substr($key, 0, 1)) {
                    case '*':$value = "<b>$value</b>";
                        break;
                    case '/':$value = "<i>$value</i>";
                        break;
                    case '_':$value = "<u>$value</u>";
                        break;
                }
            }
            $string = str_replace($key, $value, $string);
        }
        return $string;
    }

    /**
     * Convert special characters to HTML entities
     * @param string $string String
     * @return string
     */
    private static function specialChars($string) {
        return htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
    }

}
