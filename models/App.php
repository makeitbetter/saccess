<?php

/**
 * Application
 */
class App {

    private static
    /**
     * @var boolean Is initiated
     */
    $isInitiated = false;

    /**
     * Initiate
     * @return void
     */
    public static function init() {
        if (self::$isInitiated) {
            return;
        }
        self::$isInitiated = true;
        if (Request::getQueryParam('m') !== 'load') {
            Layout::init();
        }
        if (User::getId()) {
            switch ($module = Request::getQueryParam('m', 'dir')) {
                case 'dir':
                    $path = Request::getQueryParam('item', Request::getCookie('i', ROOT));
                    self::addModule('dir', Dir::getItem($path));
                    Request::setCookie('i', $path, '+ 7 days');
                    break;
                case 'file': self::addModule('file', File::getItem(Request::getQueryParam('item', ROOT . '/index.php')));
                    break;
                case 'db': self::addModule('file', File::getItem(SACCESS . '/adminer.php'));
                    break;
                default:self::addModule($module, []);
            }
        }
        Layout::renderPage();
    }

    /**
     * Get config
     * @param string $param Variable path
     * @param mixed $default Default value
     * @return mixed
     */
    public static function config($param, $default = null) {
        $items = explode('/', $param);
        $param = array_shift($items);
        if (!isset($GLOBALS[$param])) {
            return $default;
        }
        if (!count($items)) {
            return $GLOBALS[$param];
        }
        $result = $GLOBALS[$param];
        foreach ($items as $item) {
            if (!isset($result[$item])) {
                return $default;
            }
            $result = $result[$item];
        }
        return $result;
    }

    /**
     * Reload page
     * @param string Location
     */
    public static function reloadPage($location = null) {
        if (!$location) {
            $location = $_SERVER['REQUEST_URI'];
        }
        header('Location: ' . $location);
        exit;
    }

    /**
     * Add alerts message
     * @param string $text Message text
     * @param string $mode Message type
     */
    public static function addAlerts($text, $mode = 'success') {
        Request::appendSession('a', ['t' => $text, 'm' => $mode]);
    }

    /**
     * Print alerts messages
     * @return void
     */
    public static function printAlerts() {// print alerts
        if (empty(Request::getSession('a'))) {
            return;
        }
        $alerts = [];
        foreach (Request::getSession('a', []) as $message) {
            if (isset($alerts[$message['m']])) {
            	$alerts[$message['m']][] = $message['t'];
            } else {
            	$alerts[$message['m']] = [$message['t']];
            }
        }
        echo '<div class="alerts">';
        foreach ($alerts as $mode => $texts) {
            echo '<div class="alert alert-' . $mode . '"><a href="#" class="close">×</a>';
            foreach ($texts as $text) {
            	echo '<div>' . $text . '</div>';
            }
            echo '</div>';
        }
        echo '</div>';
        Request::setSession('a', []);
    }

    /**
     * Add module
     * @param string $name Module name
     * @param array $args arguments
     */
    public static function addModule($name, ...$args) {
        $object = self::getModuleObject($name, ...$args);
        if ($object) {        
            $object->init();
        }
    }

    public static function getVersion() {
        return 'v0.0.8';
    }
    
    private static function getModuleObject($name, ...$args) {
    	switch ($name) {
            case 'audio': return new ModuleAudio($args[0]);
            case 'dir': return new ModuleDir($args[0]);
            case 'load': return new ModuleDownload();
            case 'file': return new ModuleFile($args[0]);
            case 'image': return new ModuleImage($args[0]);
            case 'media': return new ModuleMedia();
            case 'path': return new ModulePath($args[0]);
            case 'switch': return new ModuleSwitch($args[0]);
            case 'syntax': return new ModuleSyntax($args[0]);
            case 'user': return new ModuleUser();
            case 'video': return new ModuleVideo($args[0]);
        }
        return null;
    }

}