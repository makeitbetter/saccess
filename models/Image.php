<?php

/**
 * Image
 */
class Image extends File {

    /**
     * @var array<Image> Collection
     */
    public static $collection;

    /**
     * @var array Image data
     */
    private $info;

    /**
     * Get width
     * @return int
     */
    public function getWidth() {
        return intval($this->getInfo(0));
    }

    /**
     * Get height
     * @return int
     */
    public function getHeight() {
        return intval($this->getInfo(1));
    }

    /**
     * Get data
     * @param int $index Parameters number
     * @return mixed
     */
    private function getInfo($index) {
        if (!$this->info) {
            $this->info = getimagesize($this->path);
        }
        return $this->info[$index];
    }

}
