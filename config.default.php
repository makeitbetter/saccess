<?php

$users = [
    'admin' => '21232f297a57a5a743894a0e4a801fc3',
    'manager' => [
        'password' => '3915b35274b63cc340fe3d7f0bad27ed',
        'access' => [ROOT],
        'deny' => [SACCESS],
    ],
];

$db = [
    'driver' => 'mysql',
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'dbname' => '',
    'charset' => 'utf8',
];

$file_types = [
    'php' => ['php', 'module', 'inc', 'tpl'],
    'txt' => ['txt', 'fb2', 'log', 'ini', 'htaccess', 'json', 'md', 'bat', 'sh'],
    'sql' => ['sql'],
    'xml' => ['xml'],
    'js' => ['js', 'ts'],
    'css' => ['css', 'scss'],
    'html' => ['html', 'htm'],
    'image' => ['gif', 'png', 'jpg', 'jpeg', 'ico'],
    'audio' => ['mp3', 'wma', 'ac3', 'wav'],
    'video' => ['mp4', 'mov', 'wav', 'avi', 'divx', 'dvd', 'wmv'],
];

$encodes = [
    'utf-8' => true,
    'cp1251' => false,
    'koi8-r' => false,
    'ibm866' => false,
];

$codestyle = [
    'indent unit' => 2,
    'tab size' => 2,
    'tabs to space' => true,
    'theme' => 'default', // Choose other in https://codemirror.net/theme/
];

/*
 * CodeMirror CDN
 */
// $codemirror='//cdnjs.cloudflare.com/ajax/libs/codemirror/5.5.0';