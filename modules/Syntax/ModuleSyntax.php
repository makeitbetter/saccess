<?php

/**
 * Edit file syntax module
 */
class ModuleSyntax {

    /**
     * @var File File ooject
     */
    private $file;    
    
    /**
     * @var string Encode
     * @var array Encodes list
     * @var string Content
     * @var string Edit mode
     */
    private $encode, $encodes, $content, $mode;
    
    public function __construct($file) {
    	$this->file = $file;
    }

    /**
     * Initiate
     * @return void
     */
    public function init() {
        Layout::$title = '📄 ' . $this->file->name;
        $this->encodes = App::config('encodes', []);
        $content = $this->file->read();
        $this->encode = 'utf-8';
        if (($encode = Request::getQueryParam('cp')) && isset($this->encodes[$encode])) {
            $this->encode = $encode;
            Request::setCookie('e', $encode, '+ 14 days');
        } elseif (($encode = Request::getCookie('e')) && isset($this->encodes[$encode])) {
            $this->encode = $encode;
        }
        if (isset($this->encodes[$this->encode])) {
            $content = iconv($this->encode, 'utf-8//IGNORE', $content);
        }
        $this->content = htmlspecialchars($content);
        $modes = array(
            'php' => '\'application/x-httpd-php\'',
            'js' => '\'text/javascript\'',
            'css' => '\'text/css\'',
            'html' => '\'text/html\'',
            'txt' => '\'text/plain\'',
            'xml' => '\'text/xml\'',
            'sql' => '\'text/x-mysql\'',
        );
        $this->mode = $modes['txt'];
        if (Request::getQueryParam('d') !== 'txt' && isset($modes[$this->file->getType()])) {
            $this->mode = $modes[$this->file->getType()];
        }
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        $codemirror = App::config('codemirror', '//cdnjs.cloudflare.com/ajax/libs/codemirror/5.5.0');
        $theme = App::config('codestyle/theme', 'default');
        $js = file_get_contents('scripts/syntax.js');
        Layout::addJs($js);
        include 'modules/Syntax/layout.php';
    }

}