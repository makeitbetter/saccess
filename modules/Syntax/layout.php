<link rel="stylesheet" type="text/css" href="<?= $codemirror; ?>/codemirror.css">
<?php if ($theme !== 'default') { ?>
    <link rel="stylesheet" type="text/css" href="<?= $codemirror; ?>/theme/<?= $theme; ?>.css">
<?php } ?>
<script type="text/javascript" src="<?= $codemirror; ?>/codemirror.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/css/css.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/htmlmixed/htmlmixed.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/addon/edit/matchbrackets.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/php/php.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/clike/clike.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/javascript/javascript.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="<?= $codemirror; ?>/mode/sql/sql.min.js"></script>

<form method="post" action="" id="mod-syntax_form">
    <div class="alerts"></div>
    <div class="s-controls">
        <select class="form-control inline" onchange="encode(this.value)">
            <?php foreach ($this->encodes as $encode => $isDefault) { ?>
            <option value="<?= $encode; ?>" <?= $encode === $this->encode ? 'selected' : '';?>>
                <?= $encode; ?>
            </option>
            <?php } ?>
        </select>
        <button type="button" class="btn btn-primary ic" disabled title="Save"
                onclick="submitForm(true)">Save</button>
        <button type="button" class="btn btn-primary ic" disabled title="Apply" 
                onclick="submitForm(false)">Apply</button>
        <a class="btn btn-info" title="Exit"
           href="?m=dir&item=<?= urlencode($this->file->dir); ?>" id="mod-syntax_exit">Exit</a>
    </div>

    <input type="hidden" name="cp" value="<?= $this->encode; ?>"/>
    <input type="hidden" name="p" value="<?= $this->file->path; ?>" />
    <input type="hidden" name="a" value="edit" />
    <div id="textarea">
        <textarea name="c" id="code"><?= $this->content; ?></textarea>
    </div>
</form>

<script>
    var codeStyle = {
    	mode: <?= $this->mode; ?>,
        indentUnit: <?= App::config('codestyle/indent unit', 2); ?>,
        tabSize: <?= App::config('codestyle/tab size', 2); ?>,
        indentWithTabs: <?= App::config('codestyle/tabs to space') ? 'true' : 'false'; ?>,  
        theme: '<?= $theme;?>'
    };
</script>