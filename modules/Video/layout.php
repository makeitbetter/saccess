<div id="mod-image">
    <div class="s-controls">
        <a href="?m=load&dir=<?= urlencode($this->video->dir); ?>&items=<?= urlencode($this->video->name); ?>" class="btn btn-primary" target="_blank" title="Download">Download</a>
        <a href="?m=dir&item=<?= $this->video->dir; ?>" class="btn btn-info">Exit</a>
    </div>
    
    <?php App::addModule('switch', $this->video);?>
    
    <div class="s-info">
        <video controls="controls"><source src="?m=media&item=<?= urlencode($this->video->path); ?>" /></video>
    </div>
</div>
<script>var imagesList =<?= json_encode($this->list, 512); ?>;</script>