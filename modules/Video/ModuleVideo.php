<?php

/**
 * Video file module
 */
class ModuleVideo {

    /**     *
     * @var File File object
     */
    private $video;
    
    public function __construct($video) {
    	$this->video = $video;
    }

    /**
     * Initiate
     */
    public function init() {
        Layout::$title = '🎞 ' . $this->video->name;
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        include 'modules/Video/layout.php';
    }

}