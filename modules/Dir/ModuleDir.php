<?php

/**
 * Directory module
 */
class ModuleDir {

    /**
     * @var Dir Directory object
     */
    private $dir;

    /**
     * @var array Directory content list
     */
    private $list = [];
    
    public function __construct($dir) {
    	$this->dir = $dir;
    }

    /**
     * Initiate
     * @return void
     */
    public function init() {
        Layout::$title = '📂 ' . $this->dir->name ?: '/';
        if (!Request::getPostData()) {
            App::addModule('path', $this->dir);
        }
        if (!$this->dir->exists()) {
            App::addAlerts(Text::format('Folder *d does not exists.', ['*d' => $this->dir->path]), 'warning');
            return App::printAlerts();
        }
        $action = '_' . Request::getPostData('a');
        if ($action !== '_' && method_exists($this, $action)) {
            $this->$action();
            App::reloadPage();
        }
        if (!$this->list = $this->dir->read(true)) {
            App::addAlerts("Folder *{$this->dir->path} is empty or is unavailable to read.", 'warning');
            return App::printAlerts();
        }
        $this->render();
    }

    /**
     * Action Create
     */
    private function _create() {
        $name = Text::transliterate(Request::getPostData('f'));
        $type = Request::getPostData('t', 'file');
        $info = pathinfo($name);
        $query = '';
        $error = false;
        if ($type === 'file') {
            $item = File::getItem($this->dir->path . I. $name);
            if ($item->exists()) {
                App::addAlerts(Text::format('File *f already exists.', ['*f' => $name]), 'warning');
            }
            elseif (Request::getPostData('o')) {
                $query = 'm=file&item=' . $item->path;
            }
        } else {
            $item = Dir::getItem($this->dir->path . I. $name);
            if ($item->exists()) {
                App::addAlerts(Text::format('Folder *d already exists.', ['*d' => $name]), 'warning');
                $error = true;
            }
            elseif (Request::getPostData('o')) {
                $query = 'm=dir&item=' . $item->path;
            }
        }
        if (!$error) {
            if ($item->create()) {
                $type = $item->getType() === 'directory' ? 'Folder' : 'File';
                App::addAlerts(Text::format('$t *f has been created successfully.', ['*f' => $item->name, '$t' => $type]), 'success');
            } else {
                $message = $type === 'file' ? 'Unable to create file *f.' : 'Unable to create folder *f.';
                App::addAlerts(Text::format($message, ['*f' => $item->name]), 'danger');
                $query = '';
            }
        }
        App::reloadPage('?' . Request::updateQueryParams($query));
    }

    /**
     * Action Copy
     */
    private function _copy() {
        $error = false;
        foreach (Request::getPostData('file', []) as $oldName => $newName) {
            $oldFile = File::getItem("{$this->dir->path}/$oldName");            
            if ($oldFile->copy($newName)) {
                App::addAlerts(Text::format('File *o has been successfully copied into *n.', ['*o' => $oldName, '*n' => $newName]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to copy *o into *n.', ['*o' => $oldName, '*n' => $newName]), 'danger');
            }
        }
        foreach (Request::getPostData('dir', []) as $oldName => $newName) {
            $oldDir = Dir::getItem("{$this->dir->path}/$oldName");
            if ($oldDir->copy($newName)) {
                App::addAlerts(Text::format('Folder *o has been successfully copied into *n.', ['*o' => $oldName, '*n' => $newName]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to copy *o into *n.', ['*o' => $oldName, '*n' => $newName]), 'danger');
            }
        }
        App::reloadPage();
    }
    
    /**
     * Action Move
     */
    private function _move() {
        $error = false;
        foreach (Request::getPostData('file', []) as $oldName => $newName) {
            $oldFile = File::getItem("{$this->dir->path}/$oldName");
            if ($oldFile->rename($newName)) {
                App::addAlerts(Text::format('File *o has been successfully moved to *n.', ['*o' => $oldName, '*n' => $newName]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to move *o to *n.', ['*o' => $oldName, '*n' => $newName]), 'danger');
            }
        }
        foreach (Request::getPostData('dir', []) as $oldName => $newName) {
            $oldDir = Dir::getItem("{$this->dir->path}/$oldName");
            if ($oldDir->rename($newName)) {
                App::addAlerts(Text::format('Folder *o has been successfully moved to *n.', ['*o' => $oldName, '*n' => $newName]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to move *o to *n.', ['*o' => $oldName, '*n' => $newName]), 'danger');
            }
        }
        App::reloadPage();
    }

    /**
     * Action Delete
     */
    private function _delete() {
        foreach (Request::getPostData('file', []) as $name) {
            $file = File::getItem($this->dir->path . I. $name);
            if ($file->delete()) {
                App::addAlerts(Text::format('File *n has been deleted successfully.', ['*n' => $name]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to delete file *n.', ['*n' => $name]), 'danger');
            }
        }
        foreach (Request::getPostData('dir', []) as $name) {
            $dir = Dir::getItem($this->dir->path . I. $name);
            if ($dir->delete()) {
                App::addAlerts(Text::format('Folder *n has been deleted successfully.', ['*n' => $name]), 'success');
            } else {
                App::addAlerts(Text::format('Unable to delete folder *n.', ['*n' => $name]), 'danger');
            }
        }
        App::reloadPage();
    }

    /**
     * Action Upload file
     * @return void
     */
    private function _upload() {
        if (!$file = Request::getFiles('file')) {
            return App::addAlerts('No one file was uploaded.', 'warning');
        }
        foreach ($file->name as $index => $name) {
            if ($file->error[$index]) {
                if ($name) {
                    App::addAlerts(Text::format('File *n uploading error.', ['*n' => $name]), 'danger');
                    App::addAlerts(print_r($file, 1), 'danger');
                }
                continue;
            }
            $info = pathinfo($name);
            // Unzip after upload
            if ($info['extension'] === 'zip' && Request::getPostData('z')) {
                $zip = new ZipArchive();
                $result = $zip->open($file->tmp_name[$index]);
                if ($result === true && $zip->extractTo($this->dir->path)) {
                    $zip->close();
                    App::addAlerts(Text::format('Archive *n has been successfully uploaded and unzipped.', ['*n' => $name]), 'success');
                    continue;
                } else {
                    App::addAlerts(Text::format('Archive *n unzip error.', ['*n' => $name]), 'danger');
                    continue;
                }
            }
            //
            elseif (copy($file->tmp_name[$index], $this->dir->path . I. $name)) {
                App::addAlerts(Text::format('File *n has been successfully uploaded.', ['*n' => $name]), 'success');
                continue;
            } else {
                App::addAlerts(Text::format('File *n upload error.', ['*n' => $name]), 'danger');
                continue;
            }
        }
    }

    /**
     * Print layout
     */
    public function render() {
        App::printAlerts();
        include 'modules/Dir/layout.php';
        $js = file_get_contents('scripts/dir.js');
        Layout::addJs($js);
        $css = file_get_contents('styles/dir.css');
        Layout::addCss($css);
    }

}