<script>var dirList =<?= json_encode($this->list, 512); ?>;</script>

<input type="hidden" id="mod-dir_path" value="<?= $this->dir->path; ?>" />

<div class="s-controls" id="mod-dir_controls">
    <select class="form-control" id="mod-dir_pager" onchange="changePager(this.value)">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="0">All</option>
    </select>
    <button type="button" class="btn btn-primary" onclick="create()">Create</button>
    <button type="button" class="btn btn-primary ic" onclick="move(true)" disabled>Copy</button>
    <button type="button" class="btn btn-primary ic" onclick="move(false)" disabled>Move</button>
    <button type="button" class="btn btn-primary ic" onclick="remove()" disabled>Delete</button>
    <button type="button" class="btn btn-primary" onclick="upload()">Upload</button>
    <button type="button" class="btn btn-primary ic"  onclick="download()" disabled>Download</button>
</div>

<ul style="display: none;" class="pagination nav-pages p" id="mod-dir_pagination"></ul>

<table class="table" id="mod-dir_table">
    <thead>
        <tr>
            <th>
                <span><input type="checkbox" onclick="checkAll(this.checked)" /></span>
            </th>
            <th class="col-name"><a onclick="sortBy('name')">Name</a></th>
            <th class="col-size text-right"><a onclick="sortBy('size')">Size</a></th>
            <th class="col-time text-right"><a onclick="sortBy('time')">Modified</a></th>
        </tr>
    </thead>
    <tbody id="mod-dir_tbody">
    </tbody>
</table>