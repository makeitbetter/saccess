<?php

/**
 * File path module
 */
class ModulePath {

    /**
     * @var Dir Directory or file object
     */
    private $item;

    /**
     * @var array Path elements list
     */
    private $paths;
    
    public function __construct($item) {
    	$this->item = $item;
    }

    /**
     * Initiate
     */
    public function init() {
        $paths = [];
        $parent = $this->item->getParent();
        if ($parent) {
            do {
                $paths[] = [$parent->name, $parent->hasAccess()];
            } while (count($paths) < 20 && $parent = $parent->getParent());
        }
        $this->paths = array_reverse($paths);
        if (count($this->paths) && !$this->paths[0][0]) {
            /* $this->paths[0][0]='/';*/
        }
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        $path = '';
        echo '<div class="p"><div class="p">';
        foreach ($this->paths as $key => $value) {
            if ($key || $value) {
                $path .= $value[0] . I;
            }
            echo ($value[1] ? '<a href="?m=dir&item=' . $path . '">' . ($value[0] ?: 'ROOT') . '</a>' : ($value[0] ?: 'ROOT')) . ' » ';
        }
        $path .= $this->item->name;
        echo '<strong>' . ($this->item->name ?: 'ROOT') . '</strong></div>';
        echo '<div>';
        $uri = $this->item->getUri();
        if ($uri !== null) {
            echo '<a href="' . $uri . '" target="_blank">http://' . $_SERVER['HTTP_HOST'] . $uri . '</a>';
        } else {
            echo 'Is not available via HTTP';
        }
        echo '</div></div>';
    }

}