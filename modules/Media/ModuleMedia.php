<?php

/**
 * Flush media files
 */
class ModuleMedia {

    /**
     * Initiate
     */
    public function init() {
        $item = Dir::getItem(Request::getQueryParam('item'));
        if (!$item->hasAccess()) {
            die('<h3>Forbidden: Access is denied</h3>');
        }
        if (is_file($item->path)) {
            header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
            header('Content-Type: ' . mime_content_type($item->path));
            header('Last-Modified: ' . gmdate('r', filemtime($item->path)));
            header('Content-Length: ' . (filesize($item->path)));
            $file = fopen($item->path, 'r');
            while (!feof($file)) {
                echo fread($file, 1024);
                flush();
            }
            fclose($file);
        }
        exit;
    }

}