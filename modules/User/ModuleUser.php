<?php

/**
 * User authentication module
 */
class ModuleUser {

    /**
     * Initiate
     */
    public function init() {
        if (Request::getPostData('l')) {
            User::login(Request::getPostData('l'), Request::getPostData('p'));
            App::reloadPage();
        } elseif (Request::getPostData('o')) {
            User::logout();
            App::reloadPage('/');
        }
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        include 'modules/User/layout.php';
    }

}