<form action="" method="post" id="mod-user">

    <?php if (!User::getId()) { ?>
        <?php App::printAlerts(); ?>
        <div class="p text-left">
            <label>Username</label>
            <input type="text" name="l" autocomplete="off" class="form-control" required autofocus />
        </div>
        <div class="p text-left">
            <label>Password</label>
            <input type="password" name="p" class="form-control" required/>
        </div>
        <input type="submit" value="Log in" class="btn btn-primary" title="Login"/>

    <?php } else { ?>
        <input type="submit" name="o" value="Logout" class="btn btn-default pull-left" style="margin-right: 15px;"/>
        <div style="padding: 8px 0;">
            <?= User::getId(); ?>
        </div>
    <?php } ?>

</form>