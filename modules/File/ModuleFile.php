<?php

/**
 * Files core module
 */
class ModuleFile {

    /**
     * @var File File object
     */
    private $file;

    /**
     * @var string Handler module name
     */
    private $handler;
    
    public function __construct($file) {
    	$this->file = $file;
    }

    /**
     * initiate
     * @return void
     */
    public function init() {
        Layout::$title = '📄 ' . $this->file->name;
        if (!Request::getPostData()) {
            App::addModule('path', $this->file);
        }
        if (!$this->file->exists()) {
            App::addAlerts(Text::format('File *f is not found', ['*f' => $this->file->path]), 'warning');
            return App::printAlerts();
        }
        if (!$this->file->hasAccess()) {
            App::addAlerts(Text::format(Dir::ACCESS_DENIED_MESSAGE, ['*d' => $this->file->path]), 'warning');
            return App::printAlerts();
        }
        $action = '_' . Request::getPostData('a');
        if ($action !== '_') {
            if (User::isGuest()) {
                $result['error'][] = 'Session expired.<br/>The changes are not saved..';
            } elseif (Request::getPostData('p') !== $this->file->path) {
                $result['error'][] = 'Server request error.<br/>The changes are not saved..';
            } else {
                if (method_exists($this, $action)) {
                    $result = $this->$action();
                }
            };
            echo json_encode($result);
            exit;
        }

        $this->handler = $this->file->getHandler();
        if (Request::getQueryParam('d') === 'txt') {
            $this->handler = 'syntax';
        }
        $this->render();
    }

    /**
     * Action Edit syntax
     * @return array
     */
    private function _edit() {
        $content = Request::getPostData('c');
        // Encode
        if (($encode = Request::getPostData('cp')) && $encode != 'utf-8') {
            $content = @iconv('utf-8', $encode . '//TRANSLIT', $content);
        }
        $result = $this->file->write($content);
        if (!$result) {
            return ['error' => ['Failed to update the file']];
        }
        return ['success' => ['The file has been successfully updated']];
    }

    /**
     * Print layout
     */
    public function render() {
        App::printAlerts();
        echo '<script type="text/javascript">window.currentFilePath = "' . $this->file->path . '";</script>';
        if ($this->handler) {
            App::addModule($this->handler, $this->file);
        } else {
            include 'modules/File/layout.php';
        }
    }

    /**
     * Print file information
     * @var File $file File object
     */
    public function printInfo() {
        $classes = [
        	'icon big-icon',
            'icon-' . $this->file->getIcon(),
            'type-' . $this->file->getType(),
            'ext-' . $this->file->extension,
        ];
        echo '<figure class="p"><span class="' . implode(' ', $classes) . '"></span><b>' . $this->file->extension . '</b></figure>';
        echo '<div>File name: ' . $this->file->name . '</div>';
        echo '<div>File size: ' . Text::getBytes($this->file->getFileSize()) . '</div>';
        echo '<div>Modified: ' . date('Y-m-d H:i:s', $this->file->getModifyTime()) . '</div>';
    }

}