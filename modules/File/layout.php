<div id="mod-file">
    <div class="s-controls">
        <a href="?<?= Request::updateQueryParams(['d' => 'txt']); ?>" class="btn btn-primary" title="Edit as text">As text</a>
        <a href="?m=load&dir=<?= urlencode($this->file->dir); ?>&items=<?= urlencode($this->file->name); ?>" class="btn btn-primary" target="_blank" title="Download">Download</a>
        <a href="?m=dir&item=<?= urlencode($this->file->dir); ?>" class="btn btn-info">Back</a>
    </div>
    <div class="s-info">
        <?php $this->printInfo(); ?>
    </div>
</div>