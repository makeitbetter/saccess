<?php

/**
 * File download module
 */
class ModuleDownload {

    /**
     * Initiate
     */
    public function init() {
        $directory = Dir::getItem(Request::getQueryParam('dir'));
        if (!$directory->hasAccess()) {
            die('<h3>Forbidden: Access is denied</h3>');
        }

        $files = explode(',', Request::getQueryParam('items'));
        if (count($files) == 1 && is_file($directory->path . I. $files[0])) {
            $this->flush($directory->path . I. $files[0]);
        } elseif (count($files) > 1 || is_dir($directory->path . I. $files[0])) {
            $path = SACCESS . I. date('ymd-His') . '.zip';
            $zip = new ZipArchive;
            if ($zip->open($path, ZipArchive::CREATE) === true) {
                foreach ($files as $file) {
                    if (is_file($directory->path . I. $file)) {
                        $zip->addFile($directory->path . I. $file, $file);
                    } elseif (is_dir($directory->path . I. $file)) {
                        $files2 = $this->allFiles($directory->path, $file);
                        foreach ($files2 as $file2) {
                            $zip->addFile($directory->path . I. $file2, $file2);
                        }
                    }
                }
                $zip->close();
                $this->flush($path);
            }
            unlink($path);
        }
        exit;
    }

    /**
     * Flush files
     * @param string $fileName File name
     * @param string $mimeType MIME type
     */
    private function flush($fileName, $mimeType = 'application/octet-stream') {
        if (file_exists($fileName)) {
            header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
            header('Content-Type: ' . $mimeType);
            header('Last-Modified: ' . gmdate('r', filemtime($fileName)));
            header('ETag: ' . sprintf('%x-%x-%x', fileinode($fileName), filesize($fileName), filemtime($fileName)));
            header('Content-Length: ' . (filesize($fileName)));
            header('Connection: close');
            header('Content-Disposition: attachment; filename="' . basename($fileName) . '";');
            $file = fopen($fileName, 'r');
            while (!feof($file)) {
                echo fread($file, 1024);
                flush();
            }
            fclose($file);
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
            header('Status: 404 Not Found');
        }
    }

    /**
     * Get all files
     * @param string $path Path
     * @param string $name Name
     * @return array
     */
    private function allFiles($path, $name) {
        $list = [];
        $dir = opendir("$path/$name");
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            if (is_file("$path/$name/$file")) {
                $list[] = "$name/$file";
            }
            elseif (is_dir("$path/$name/$file")) {
                $list = array_merge($list, $this->allFiles($path, "$name/$file"));
            }
        }
        return $list;
    }

}