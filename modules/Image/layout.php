<div id="mod-image">
    <div class="s-controls">
        <a href="?m=load&dir=<?= urlencode($this->image->dir); ?>&items=<?= urlencode($this->image->name); ?>" class="btn btn-primary" target="_blank" title="Download">Download</a>
        <a href="?m=dir&item=<?= $this->image->dir; ?>" class="btn btn-info">Exit</a>
    </div>
    
    <?php App::addModule('switch', $this->image);?>
    
    <div class="s-info">
        <p>Image size: <?= $this->image->getWidth(); ?> X <?= $this->image->getHeight(); ?></p>
    </div>
    <img src="?m=media&item=<?= urlencode($this->image->path); ?>" />
</div>