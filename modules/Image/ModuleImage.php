<?php

/**
 * Browse image module
 */
class ModuleImage {

    /**
     * @var Image File object
     */
    private $image;
    
    public function __construct($file) {
    	$this->image = Image::getItem($file->path);
    }

    /**
     * Initiate
     */
    public function init() {
        Layout::$title = '🖼 ' . $this->image->name;
        $this->render();
    }

    public function render() {
        include 'modules/Image/layout.php';
    }

}