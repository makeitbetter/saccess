<div id="mod-image">
    <div class="s-controls">
        <a href="?m=load&dir=<?= urlencode($this->file->dir); ?>&items=<?= urlencode($this->file->name); ?>" class="btn btn-primary" target="_blank" title="Download">Download</a>
        <a href="?m=dir&item=<?= $this->file->dir; ?>" class="btn btn-info">Exit</a>
    </div>
    
    <?php App::addModule('switch', $this->file);?>
    
    <div class="s-info">
        <audio src="?m=media&item=<?= $this->file->path; ?>" controls="controls"></audio>
    </div>
</div>