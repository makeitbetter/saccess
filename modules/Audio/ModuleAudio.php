<?php

/**
 * Audio file module
 */
class ModuleAudio {

    /**
     * @var File File object
     */
    private $file;
    
    public function __construct($file) {
    	$this->file = $file;
    }

    /**
     * Initiate
     */
    public function init() {
        Layout::$title = '🎵 ' . $this->file->name;
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        include 'modules/Audio/layout.php';
    }

}