<?php

/**
 * Switch items module
 */
class ModuleSwitch {

    /**
     * @var File File object
     */
    private $file;
    
    /**
     * @var File Preveous item file object
     * @var File Next item file object
     * @var array items list
     */
    private $prev, $next, $list = [];
    
    public function __construct($file) {
    	$this->file = $file;
    }

    /**
     * Initiate
     */
    public function init() {
        $this->list = array_values(array_filter($this->file->getParent()->read(true)['files'], function($item) {
                    return $item['type'] === $this->file->getType();
                }));
        $this->render();
    }

    /**
     * Print layout
     */
    public function render() {
        $js = file_get_contents('scripts/switch.js');
        Layout::addJs($js);
        include 'modules/Switch/layout.php';
    }

}