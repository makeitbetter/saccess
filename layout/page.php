<div id="s-page">
    <header class="text-right">
        <div class="pull-left" id="s-mobile-menu">☰</div>
        <?= Layout::getHostLink();?>
    </header>
    <main>
        <menu>
            <div>
                <?php self::renderMenu(); ?>
            </div>
            <div>
                <?php App::addModule('user'); ?>
            </div>
            <div>sAccess <?= App::getVersion();?></div>
        </menu>
        <article>
            <?= $content; ?>
        </article>
    </main>
</div>
<?php self::renderModal(); ?>