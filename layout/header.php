<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="shortcut icon" type="image/x-icon" href="?m=favicon"/>
        <title><?= self::$title; ?></title>
        <style>
            <?= implode("\n", self::$cssPaths); ?>
        </style>
    </head>
    <body>