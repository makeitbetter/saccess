<div id="s-menu">
    <ul class="nav">
        <li class="<?= Request::getQueryParam('i') === SACCESS . I. 'config.php' ? '' : 'active'; ?>">
            <a href="?m=dir">File Manager</a>
        </li>
        <li>
            <a href="?<?= Request::getCookie('db', 'm=db&username=' . App::config('db/username', 'root') . '&db=' . App::config('db/dbname')); ?>">Database</a>
        </li>
        <li class="<?= Request::getQueryParam('i') === SACCESS . I. 'config.php' ? 'active' : ''; ?>">
            <a href="?m=file&item=<?= SACCESS . I ?>config.php">Configuration</a>
        </li>
    </ul>
</div>