# sAccess

[![N|Solid](http://makeitbetter.su/images/projects/sAccess-xs.png)](http://makeitbetter.su/projects/saccess)

sAccess is a file manager written in PHP, which provide easy and secure access to your server file system via HTTP. sAccess:

- can be placed in any folder accessible via HTTP;
- can be configured for different users with the delineation of rights;
- allows to work with files of any encoding;
- is integrated with the popular database manager Adminer;
- use versatile text editor CodeMirror and allows to define code style.

### Installation

[Download](http://makeitbetter.su/downloads/saccess.zip) the latest sAccess archive and unzip it in any folder on your server accessible via HTTP.

Archive contains 3 files:

- **saccess.php** - entry point of your requests. You can rename it at will
- **adminer.php** - database manager for MySQL. You can replace it with any other one from the [official website](https://www.adminer.org/)
- **conig.default.php**. Change its parameters and rename to *config.php*

### Configure

config.php should define following global variables

- **$users** - array of the users, which keys are *usernames* and values are md5 hash of their *passwords*.

If you want to limit users access then the value should contains array with keys:
- *password* - md5 hash of password
- *access* - list of the folders that will be accessable for user
- *deny* - list of the folders which access will be denied

You can use constants: **ROOT** - document root, **SACCESS** - root of the sAccess.

For example:
```
$users = [
  'manager' => [
    'password' => '3915b35274b63cc340fe3d7f0bad27ed',
    'access' => [ROOT],
    'deny' => [SACCESS],
  ],
];
```
-- define user with credentials *manager/manager* who has access to all files down from document root, exclude sAccess folder.

- **$db** - array of the parameters for database connection
    - *driver*
    - *host*
    - *username*,
    - *password*
    - *dbname*
    - *charset*

- **$file_types** - array of the used in the sAccess file types which values are list of extensions associated with type.

- **$encodes** - array of the used encodes. Encode which value equals *true* will be used as default

- **$codestyle** - options for the text editor.
    - *indent unit*
    - *tab size*
    - *tabs to space*
    - *theme* - theme of CodeMirror. You can choose it in [official website](https://codemirror.net/theme/)
];

You also can change default CDN for CodeMirror by defining variable **$codemirror**

### Compilation

You can customize code and compile it by your self. For this run the *compiler.php*. It will rebuild *saccess.php* file and create new archive *saccess.zip*