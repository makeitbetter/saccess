<?php

function adminer_object() {
	
    class sAdminer extends Adminer {
        
        public function credentials() {
            if (!User::getId()) {
                return[];
            }
            return [App::config('db/host'), App::config('db/username'), App::config('db/password')];
        }

        public function database() {
            if (!User::getId()) {
                return'';
            }
            return App::config('db/dbname');
        }

        public function name() {
            return '<ul class="nav">'
                    . '<li><a href="?m=dir">File Manager</a></li>'
                    . '<li class="active"><a href="?m=db">Database</a></li>'
                    . '<li><a href="?m=file&item=' . SACCESS . I. 'config.php">Configuration</a></li>'
                    . '</ul>'
                    . '<p>' . (User::getId() ? App::config('db/username') : '') . '</p>';
        }

        public function head() {
            echo '<link href="?m=dbstyles" rel="stylesheet">';
            echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
            echo '<link rel="shortcut icon" type="image/x-icon" href="?m=favicon"/>';
            echo '<script src="?m=dbscripts"></script>';
            return true;
        }

    }

    return new sAdminer();
}