const $sortBy = $getStorage('md.s', 'name');
const $sortOrder = $getStorage('md.sd', 1);
itemsList = itemsList.sort(($a, $b) => ($a[$sortBy] > $b[$sortBy] ? 1 : -1) * $sortOrder);

const $name = $byId('mod-switch_item').innerText;
const $index = itemsList.findIndex($elm => $elm.name === $name);
if ($index > 0) {
    const $prev = itemsList[$index - 1];
    $byId('mod-switch_prev').innerHTML = $ref('&laquo; ' + $prev.name, {href: '?m=file&item=' + $prev.path});
}
if ($index < itemsList.length - 1) {
    const $next = itemsList[$index + 1];
    $byId('mod-switch_next').innerHTML = $ref($next.name + ' &raquo;', {href: '?m=file&item=' + $next.path});
}

$keydown({
	37: () => {
    	const $prev = $findOne('#mod-switch_prev a');
        if ($prev) {
        	location.href = $prev.getAttribute('href');
        }
    },
    39: () => {
    	const $next = $findOne('#mod-switch_next a');
        if ($next) {
        	location.href = $next.getAttribute('href');
        }
    },
});