const $document = document;
$document.addEventListener('DOMContentLoaded', function() {
    
    const $header = $document.createElement('div');
    $header.id = 's-header';
    $header.innerHTML = '<div class="pull-left" id="s-mobile-menu">☰</div>' + hostLink;
    $document.body.insertBefore($header, $document.body.firstChild);
        
    $document.getElementById('s-mobile-menu').addEventListener('click', () => {
         $document.body.classList.toggle('open')
    });
    
    $document.title = '🗄 ' + $document.getElementsByTagName('h2')[0].innerText;

    const $loginForm = $document.querySelector('body > form');
    $document.querySelector('#menu > h1 > ul').after($loginForm);

    if ($document.body.clientWidth < 768) {
        const tds = $document.querySelectorAll('#table td[id^=val]');
        for (let i = 0; i < tds.length; i++) {
            const f = tds.item(i).id.split('[').pop().replace(']', '');
            tds.item(i).setAttribute('data-f', f);
        }
    }

});

let $currScroll = false;
$document.addEventListener('scroll', function() {
    if ($document.scrollingElement.scrollTop > 28) {
        if (!$currScroll) {
            $document.body.classList.add('scrolled');
            $currScroll = true;
        }
    } else {
        if ($currScroll) {
            $document.body.classList.remove('scrolled');
            $currScroll = false;
        }
    }
});
