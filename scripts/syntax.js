const $saveAvailable = () => {
    $findAll('#mod-syntax_form .ic').forEach(($item) => $item.disabled = false);
};

const $saveDisable = () => {
    $findAll('#mod-syntax_form .ic').forEach(($item) => $item.disabled = true);
};

const submitForm = ($exit) => {
    if (typeof codeeditor != 'undefined') {
        $byId('code').value = codeeditor.getValue();
    }
    $ajax({
        method: 'POST',
        data: new FormData($byId('mod-syntax_form')),
        success: function ($resp) {
            const $close = $ref('×', {href: '#', 'class': 'close', onclick: "this.parentElement.style.display = 'none'"});
            const $attr = {onmouseleave: "this.style.display = 'none'", 'class': 'alert alert-'};
            if ($resp && $resp.error) {
                $attr['class'] += 'danger';
                const text = $resp.error.map($error => $tag('div', $error));
                $byClass('alerts')[0].innerHTML = $tag('div', $close + text, $attr);
                return;
            }
            if ($resp && typeof ($resp.success) === 'undefined') {
                $attr['class'] += 'danger';
                $byClass('alerts')[0].innerHTML = $tag('div', $resp + text, $attr);
                return;
            }
            if ($exit) {
                document.location = $byId('mod-syntax_exit').getAttribute('href');
            } else if ($resp && $resp.success) {
                $attr['class'] += 'success';
            	const text = $resp.success.map($error => $tag('div', $error));
                $byClass('alerts')[0].innerHTML = $tag('div', $close + text, $attr);
                $saveDisable();
            }
        },
        error: function($error) {
    		$openModal('Error!', $error.error, $cancel);
    	},
    });
};

const $offsetTop = $byId('textarea').offsetTop;
let $height = window.innerHeight - $offsetTop - 15;
if ($height < 200) $height = 200;

if (window.innerWidth >= 320) {
    var codeeditor = CodeMirror.fromTextArea($byId('code'), {
        lineNumbers: true,
        lineWrapping: true,
        matchBrackets: true,
        mode: codeStyle.mode,
        indentUnit: codeStyle.indentUnit,
        smartIndent: true,
        tabSize: codeStyle.tabSize,
        indentWithTabs: codeStyle.indentWithTabs,
        enterMode: 'keep',
        tabMode: 'shift'
    });
    
    $byClass('CodeMirror')[0].style.height = $height + 'px';
    $byClass('CodeMirror')[0].className = $byClass('CodeMirror')[0].className + ' cm-s-' + codeStyle.theme;
} else {
	$byId('code').style.height = $height + 'px';
}

function encode($value) {
	location.href += '&cp=' + $value;
}

$listen($byClass('CodeMirror'), 'keyup', function() {
    if (codeeditor.getValue() !== $byId('code').value) {
        $saveAvailable();
    } else {
        $saveDisable();
    }
});

$listen($byId('code'), 'keypress', function() {
	$saveAvailable();
});