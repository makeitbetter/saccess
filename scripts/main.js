function $byId($id) { return document.getElementById($id); }
 
function $byClass($className) {
    const $array = [];
    const $elm = document.getElementsByClassName($className);
    for (let i = 0; i < $elm.length; i++) $array.push($elm.item(i));
    return $array;
}

function $findAll($selector) {
    const $array = [];
    const $elm = document.querySelectorAll($selector);
    for (let i = 0; i < $elm.length; i++) $array.push($elm.item(i));
    return $array;
}

function $findOne($selector) { return document.querySelector($selector); }

function $listen($elm, $event, $func) {
	if (!$elm) return;
    if (Array.isArray($elm)) {
        $elm.map(($item) => $item.addEventListener($event, function() { $func.call($item); }));
    } else {
    	$elm.addEventListener($event, function() { $func.call($elm); });
    }
}

function $tag($name, $content, $options) {
	let $attr = '';
    if (!$options) {
    	$options = {};
    } 
    switch ($name) {
        case 'input': $options.type = $content; break;
        case 'img': $options.src = $content; break;
    }
    for ($key in $options) {
        if ($options[$key] !== null) {
        	$attr += ' ' + $key + '="' + $options[$key] + '"';
        }
    }
    const $result = '<' + $name + $attr;
    if (['input', 'img'].indexOf($name) !== -1) {
    	return $result + '/>';
    }
    return $result + '>' + $content + '</' + $name + '>';
}

function $input($type, $options) {
	return $tag('input', $type, $options);
}

function $ref($content, $options) {
	return $tag('a', $content, $options);
}

function $form($method, $action, $content, $options) {
	if (!$options) {
    	$options = {};
    }
    $options.method = $method.toUpperCase();
    $options.action = $action;
    return $tag('form', $content, $options);
}

const $cancel = $tag('button', 'Cancel', {'class': 'btn btn-default', onclick: 'closeModal()'});

function $keydown($data) {
	document.addEventListener('keydown', function($event) {
    	if ($data[$event.which]) {
        	$data[$event.which].call();
        }
    });
}

function $ajax($data) {
    const $xhr = new XMLHttpRequest();
    if (!$data.url) {
    	$data.url = location.href;
    }
    $xhr.open($data.method, $data.url, true);
    $xhr.onload = () => {
        if ($xhr.status === 200) {
        	if ($data.success) {
            	$data.success.call(null, JSON.parse($xhr.responseText))
            }
        }
        else if ($data.error) {
        	$data.error.call(null, $xhr);
        }
    };
    const $body = $data.data ? $data.data : null;
    $xhr.send($body);
}

function $openModal($title, $body, $footer) {
	$findOne('#s-modal .modal-title').innerHTML = $title;
	$findOne('#s-modal .modal-body').innerHTML = $body;
	$findOne('#s-modal .modal-footer').innerHTML = $footer;
    $byId('s-modal').style.display = 'block';
}

function closeModal() {
	$byId('s-modal').style.display = 'none';
}

$listen($findAll('.alert .close'), 'click', function() {
	this.parentElement.style.display = 'none';
});

$listen($findAll('.alert'), 'mouseleave', function() {
	this.style.display = 'none';
});

$listen($byId('s-mobile-menu'), 'click', function() {
	document.body.classList.toggle('open');
});

$listen(document, 'scroll', function() {
    if (document.scrollingElement.scrollTop > 28) {
        document.body.classList.add('scrolled');
    } else {
        document.body.classList.remove('scrolled');
    }
});

const $storageName = 'saccess_state';

const $storage = localStorage.getItem($storageName) ? JSON.parse(localStorage.getItem($storageName)) : {};

const $getStorage = ($key, $default) => {
    if (typeof $storage[$key] !== 'undefined') {
        return $storage[$key];
    }
    if ($default) {
        return $default;
    }
    return null;
};

const $setStorage = ($key, $value) => {
    $storage[$key] = $value;
    localStorage.setItem($storageName, JSON.stringify($storage));
};