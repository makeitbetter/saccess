function $label($content, $options) {
	return $tag('label', $content, $options);
}

function $span($content, $options) {
	return $tag('span', $content, $options);
}

function $checkbox($options, $disabled, $checked) {
    $options.disabled = $disabled ? 'disabled' : null;
   	$options.checked = $checked ? 'checked' : null;
	return $input('checkbox', $options);
}

function $buildRow ($cells) {
	let $colspan = 1;
    const $content = $cells.map(($cell, $index) => {
        const $attr = {};
        if ($cell === null) {
            $colspan++;
        	return '';
        }
    	if ($index > 1) {
        	$attr['class'] = 'text-right';
        }
        if ($colspan > 1) {
        	$attr['colspan'] = $colspan;
            $colspan = 1;
        }
        return $tag('td', $cell, $attr);
    });
    return $tag('tr', $content.join(''));
}

function $buildTable ($data, $perPage, $page, $sortBy, $sortOrder) {   
                            
    $findAll('#mod-dir_table th a').map(($item) => $item.className = '');
    $findOne('#mod-dir_table th.col-' + $sortBy + ' a').className = 's' + $sortOrder;
    
    const $pagination = $byId('mod-dir_pagination');
    const $order = ($a, $b) => ($a[$sortBy] > $b[$sortBy] ? 1 : -1) * $sortOrder;
    let $pages;
    let $dirs = $data.dirs.sort($order);
    let $files = $data.files.sort($order);
    const $dirsNum = $dirs.length;
    const $filesNum = $files.length;
    if ($perPage > 0 && ($dirsNum + $filesNum) > $perPage) {
        $pages = Math.ceil(($dirsNum + $filesNum) / $perPage);
        if ($page > $pages) {
            $page = $pages;
        }
        let $start = ($page - 1) * $perPage;
        if ($start < $dirsNum) {
            $dirs = $dirs.slice($start, $start + $perPage);
            $perPage = $start + $perPage - $dirsNum;
            $start = 0;
        } else {
            $dirs = [];
            $start = $start - $dirsNum;
        }
        if ($perPage > 0) {
            $files = $files.slice($start, $start + $perPage);
        } else {
            $files = []
        }
        const $listItems = [];
        for (let $num = 1; $num <= $pages; $num++) {
            $listItems.push($tag('li', ($num === $page ? $span($num) : $ref($num, {onclick: 'changePage(' + $num + ')'})), {'class': $num === $page ? 'active' : ''}));
        }
        $pagination.innerHTML = $listItems.join('');
        $pagination.style.display = 'block';
    }
    else {    	
        $pagination.innerHTML = '';
        $pagination.style.display = 'none';
    }

	const $rows = [];
    if ($data.parent.access) {
        const $row = $buildRow([
            null,
            $ref('.. [' + $data.parent.name + ']', {href: '?m=dir&item=' + $data.parent.path}),
            '',
            '',
        ]);
		$rows.push($row);
    }
    $dirs.map(($dir) => {
        const $row = $buildRow([
        	$label($checkbox({name: 'dir', value: $dir.name, 'class': 'cb'}, !$dir.access) + $span('', {'class': 'icon icon-' + $dir.icon})),
            $dir.access ? $ref($dir.name, {href: '?m=dir&item=' + $dir.path}) : $dir.name,
            ' ',
            $dir.date ? $dir.date.toLocaleString() : ''
        ]);
        $rows.push($row);
    });
    $files.map(($file) => {
        const $figure = $tag('figure', $span('', {'class': 'icon icon-' + $file.icon + ' type-' + $file.type + ' ext-' + $file.ext}) + $tag('b', $file.ext));
        const $row = $buildRow([
        	$label($checkbox({name: 'file', value: $file.name, 'class': 'cb'}, !$file.access) + $figure),
            $file.access ? $ref($file.name, {href: '?m=file&item=' + $file.path}) : $file.name,
            $file.size === false ? '' : $getBytes($file.size),
            $file.date ? $file.date.toLocaleString() : ''
        ]);
        $rows.push($row);
    });
    
    $byId('mod-dir_tbody').innerHTML = $rows.join('');
    
    $listen($findAll('#mod-dir_table .cb'), 'click', function() {
        const $checked = $findAll('#mod-dir_table .cb:checked').length > 0;
        $findAll('#mod-dir_controls .ic').map(($item) => $item.disabled = !$checked);
    });
}

function $getBytes($bytes) {
    if ($bytes < 1024) {
        return $bytes.toFixed(2) + ' B'
    }
    if ($bytes < 1048576) {
        return ($bytes / 1024).toFixed(2) + ' KB'
    }
    if ($bytes < 1073741824) {
        return ($bytes / 1048576).toFixed(2) + ' MB'
    }
    return ($bytes / 1073741824).toFixed(2) + ' GB';
}

['dirs', 'files'].map(($type) => {
    dirList[$type] = dirList[$type].map(($item) => {
        $item.date = $item.time ? new Date($item.time * 1000) : null;
        return $item;
    });
});
let $perPage = $getStorage('md.pp', 25);
let $sortBy = $getStorage('md.s', 'name');
let $sortOrder = $getStorage('md.sd', 1);
let $page = 1;
$byId('mod-dir_pager').value = $perPage;
        
$buildTable(dirList, $perPage, $page, $sortBy, $sortOrder);

const changePager = ($value) => {
    $page = 1;
    $perPage = $value;
    $setStorage('md.pp', $value);
	$buildTable(dirList, $perPage, $page, $sortBy, $sortOrder);
};

const sortBy = ($name) => {
	if ($name === $sortBy) {
    	$sortOrder = $sortOrder * -1;
        $setStorage('md.sd', $sortOrder);
    } else {
    	$sortBy = $name;
        $setStorage('md.s', $name);
    	$sortOrder = 1;
        $setStorage('md.sd', 1);
    }
    $buildTable(dirList, $perPage, $page, $sortBy, $sortOrder);
};

const changePage = ($newPage) => {
	$page = $newPage;
    $buildTable(dirList, $perPage, $page, $sortBy, $sortOrder);
};

const checkAll = ($checked) => {
    $findAll('#mod-dir_table .cb').map(($item) => $item.checked = $checked);
    $findAll('#mod-dir_controls .ic').map(($item) => $item.disabled = !$checked);
};

const create = () => {
    const $type = $getStorage('md.ct', 'dir');
    let $options = $tag('option', 'File', {value: 'file', selected: $type === 'file' ? 'selected' : null});
    $options += $tag('option', 'Folder', {value: 'dir', selected: $type === 'dir' ? 'selected' : null});
    const $select = $tag('select', $options, {form: 'form-create', name: 't', onchange: "$setStorage('md.ct', this.value)", 'class': 'inline form-control'});
    const $open = $checkbox({name: 'o', value: 1, onclick: "$setStorage('md.co', this.checked)"}, false, $getStorage('md.co', true));
    const $title = 'Create new ' + $select;
    const $body = $form('post', '',
            $tag('div', 
                $label('Name:') + $input('text', {name: 'f', 'class': 'form-control'}), 
            {'class': 'p'}) + 
            $input('hidden', {name: 'a', value: 'create'}) +
            $tag('div', 
                 $label($open + ' Open after create'), 
            {'class': 'checkbox'}),
        {id: 'form-create'});
    let $footer = $input('submit', {form: 'form-create', value: 'Create', 'class': 'btn btn-primary'});
    $footer += $cancel;
    $openModal($title, $body, $footer);
};

const move = ($copy) => {
    const $title = ($copy ? 'Copying' : 'Moving') + ' of Files/Folders';
    const $elms = $findAll('#mod-dir_table .cb:checked');
    if (!$elms.length) {
    	return $openModal($title, 'No one element was selected.', $cancel);    
    }
    const $list = $elms.map(($item) => $tag('div',
        	$label($item.value) + ' to' + $input('text', {
        		name: $item.getAttribute('name') + '[' + $item.value + ']',
        		value: $item.value,
        		'class': 'form-control'
    		}),                               
        {'class': 'p'})).join('');
    const $body = $form('post', '',
        	$list + $input('hidden', {name: 'a', value: $copy ? 'copy' : 'move'}), 
        {id: 'form-move'});
    	const $footer = $input('submit', {form: 'form-move', value: $copy ? 'Copy' : 'Move', 'class': 'btn btn-primary'}) + $cancel;
    $openModal($title, $body, $footer);    
};

const remove = () => {
    const $title = 'Deleting of Files/Folders';
    const $elms = $findAll('#mod-dir_table .cb:checked');
    if (!$elms.length) {
    	return $openModal($title, 'No one element was selected.', $cancel);    
    }
    let $list = '';
    for (let $i = 0; $i < $elms.length; $i++) {
        if ($i > 9 && $elms.length - $i > 3) {
            $list += '... and also ' + ($elms.length - $i) + ' items';
            break;
        }
        $list += $elms[$i].value + '<br/>';
    }
    const $inputs = $elms.map(($item) => $input('hidden', {
    	name: $item.getAttribute('name') + '[]',
        value: $item.value
    })).join('');
    const $body = $form('post', '',
        	$tag('p', 'Are you sure you want to delete these files/folders?') + 
        	$list + $inputs + $input('hidden', {name: 'a', value: 'delete'}),
    	{id: 'form-remove'});
    const $footer = $input('submit', {form: 'form-remove', value: 'Delete', 'class': 'btn btn-primary'}) + $cancel;
    $openModal($title, $body, $footer); 
};

const download = () => {
    const $elms = $findAll('#mod-dir_table .cb:checked');
    if (!$elms.length) {
    	return $openModal('Files Download', 'No one element was selected.', $cancel);    
    }
    const $path = $byId('mod-dir_path').value;
    let $url = '?m=load&dir=' + $path + '&items=';
    for (let $i = 0; $i < $elms.length; $i++) {
        if ($i) $url += ',';
        $url += $elms[$i].value;
    }
    checkAll(false);
    window.open($url);
};

const loadFiles = ($value) => {
    $list = '';
    $total = 0;
	for (let $i = 0; $i < $value.length; $i++) {
    	$list += $tag('div', $tag('b', $value[$i].name) + ', ' + $getBytes($value[$i].size));
        $total += $value[$i].size;
    }
    if ($value.length > 1) {
    	$list += $tag('div', 'Total: ' + $getBytes($total));
    }
    $byId('uploading').innerHTML = $list;
};

const upload = () => {
    const $title = 'File uploading';
    const $body = $form('post', '',
        	$input('hidden', {name: 'a', value: 'upload'}) +
            $tag('div', $input('file', {
        		name: 'file[]', 
        		multiple: 'multiple', 
        		onchange: 'loadFiles(this.files)'
    		}), {'class': 'p'}) +
            $tag('p', '', {id: 'uploading'}) +
            $label($checkbox({name: 'z', value: '1'}) + ' Unzip'),
    	{id: 'form-upload', enctype: 'multipart/form-data'});    
    const $footer = $input('submit', {form: 'form-upload', value: 'Save', 'class': 'btn btn-primary'}) + $cancel;
    $openModal($title, $body, $footer); 
};