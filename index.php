<?php

if (!$_SERVER['QUERY_STRING']) {
    header('Location: ?m=dir');
}
ini_set('display_errors', 0);
define('ROOT', realpath($_SERVER['DOCUMENT_ROOT']));
define('SACCESS', __DIR__);
$relative = substr(SACCESS, strlen(ROOT));
define('URI', $relative);
define('I', DIRECTORY_SEPARATOR);
if (is_file('config.php')) {
    require_once('config.php');
}

include_once 'db/index.php';

include_once 'models/App.php';
include_once 'models/Dir.php';
include_once 'models/File.php';
include_once 'models/Image.php';
include_once 'models/Layout.php';
include_once 'models/Request.php';
include_once 'models/Text.php';
include_once 'models/User.php';

include_once 'modules/Dir/ModuleDir.php';
include_once 'modules/User/ModuleUser.php';
include_once 'modules/Audio/ModuleAudio.php';
include_once 'modules/Download/ModuleDownload.php';
include_once 'modules/Media/ModuleMedia.php';
include_once 'modules/File/ModuleFile.php';
include_once 'modules/Image/ModuleImage.php';
include_once 'modules/Path/ModulePath.php';
include_once 'modules/Switch/ModuleSwitch.php';
include_once 'modules/Syntax/ModuleSyntax.php';
include_once 'modules/Video/ModuleVideo.php';

Request::init();
User::init();

function img($base64) {
	cacheHeaders();
    header('Content-type: image/png');
    echo base64_decode($base64); 
}

function cacheHeaders() {
	$time = 2592000;
    header('Cache-Control: max-age=' . $time);    
    header('Expires: ' . date('r', time() + $time));
}

function getStyles($data) {
    cacheHeaders();
	header('Content-type: text/css');
    echo $data;
}

function getScripts($data) {
    cacheHeaders();
	header('Content-type: application/javascript');
    echo $data;
}

if (Request::getQueryParam('m', 'db') === 'db' && is_file('adminer.php')) {
    if (Request::getQueryParam('username')) {
    	Request::setCookie('db', $_SERVER['QUERY_STRING'], '+ 7 days');
    }
    require_once('adminer.php');
} elseif (Request::getQueryParam('m') === 'favicon') {
    img(Layout::$favicon);
} elseif (Request::getQueryParam('m') === 'logo') {
    img(Layout::$logo);
} elseif (Request::getQueryParam('m') === 'dbstyles') {
    $css = file_get_contents('styles/boot.css');
    $css .= file_get_contents('styles/adminer.css');
    $css .= ' #s-header {background-color: ' . App::config('color', '#003') . ';}';
    getStyles($css);
} elseif (Request::getQueryParam('m') === 'dbscripts') {
    $js = "var hostLink = '" . Layout::getHostLink() . "';\n";
	$js .= file_get_contents('scripts/db.js');
    getScripts($js);
} else {	
    App::init();
}